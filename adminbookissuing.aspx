﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BoovieNested.Master" AutoEventWireup="true" CodeBehind="adminbookissuing.aspx.cs" Inherits="BoovieStore.adminbookissuing" %>

<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $(".table").prepend($("<thead></thead>").append($(this).find("tr:first"))).dataTable();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <main id="wrapper">
        <section class="has-margin-size-5">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-5">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <div class="element-center">
                                            <h4 class="d-block mx-auto">Book Issuing</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="element-center">
                                            <img width="100" src="imgs/books.png"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <hr />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>User ID</label>
                                        <div class="mb-3">
                                            <asp:TextBox class="form-control" ID="User_ID" runat="server" placeholder="ID"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Book ID</label>
                                        <div class="mb-3">
                                            <div class="input-group">    
                                                <asp:TextBox class="form-control" ID="Book_ID" runat="server" placeholder="ID" TextMode="Number"></asp:TextBox>
                                                <asp:Button class="btn btn-primary" ID="Go" runat="server" Text="Go" OnClick="Go_Click" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>User Name</label>
                                        <div class="mb-3">
                                            <asp:TextBox class="form-control" ID="User_Name" runat="server" placeholder="User Name" ReadOnly="True"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Book Name</label>
                                        <div class="mb-3">
                                            <asp:TextBox class="form-control" ID="Book_Title" runat="server" placeholder="Book Title" ReadOnly="True"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Start Date</label>
                                        <div class="mb-3">
                                            <asp:TextBox class="form-control" ID="Issue_Date" runat="server" TextMode="Date"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>End Date</label>
                                        <div class="mb-3">
                                            <asp:TextBox class="form-control" ID="Due_Date" runat="server" TextMode="Date"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 d-grid">
                                        <asp:Button class="btn w-100 btn-primary btn-lg" ID="Issue" runat="server" Text="Issue" OnClick="Issue_Click" />
                                    </div>  
                                    <div class="col-6 d-grid">
                                        <asp:Button class="btn w-100 btn-success btn-lg" ID="Return" runat="server" Text="Return" OnClick="Return_Click" />
                                    </div>  
                                </div>
                            </div>
                        </div>
                        <a href="homepage.aspx" class="btn-back"><i class="fas fa-long-arrow-alt-left"></i> Back to Homepage</a>
                    </div>            
                    <div class="col-md-7">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <div class="element-center">
                                            <h4>Issued Book List</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <hr />
                                    </div>
                                </div>
                                <div class="row">
                                    <asp:SqlDataSource ID="Issued_Source" runat="server" ConnectionString="<%$ ConnectionStrings:BoovieStoreConnectionString %>" SelectCommand="SELECT * FROM [BookIssuing]"></asp:SqlDataSource>
                                    <div class="col">
                                        <asp:GridView class="table table-striped table-bordered hover cell-border" ID="Issued" runat="server" AutoGenerateColumns="False" DataSourceID="Issued_Source" OnRowDataBound="Issued_RowDataBound">
                                            <Columns>
                                                <asp:BoundField DataField="user_id" HeaderText="User ID" SortExpression="user_id" />
                                                <asp:BoundField DataField="user_name" HeaderText="User Name" SortExpression="user_name" />
                                                <asp:BoundField DataField="book_id" HeaderText="Book ID" SortExpression="book_id" />
                                                <asp:BoundField DataField="book_name" HeaderText="Book Title" SortExpression="book_name" />
                                                <asp:BoundField DataField="issue_date" HeaderText="Issue Date" SortExpression="issue_date" />
                                                <asp:BoundField DataField="due_date" HeaderText="Due Date" SortExpression="due_date" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
</asp:Content>
