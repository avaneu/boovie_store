﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BoovieStore
{
    public partial class adminpublishermanagement : System.Web.UI.Page
    {
        string conn = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            string sessionName = Session["name"] as string;
            string sessionRole = Session["role"] as string;
            if (string.IsNullOrEmpty(sessionName)) {
                Response.Write("<script>alert('You are not logged in. Please login again to continue.');</script>");
                Response.Redirect("userlogin.aspx");
            }
            else if (sessionRole == "user") {
                Response.Redirect("userprofile.aspx");
            }
            Publishers.DataBind();
        }
        protected void Go_Click(object sender, EventArgs e)
        {
            GetPublisherByID();
        }
        protected void Add_Click(object sender, EventArgs e)
        {
            if (DoesPublisherExist())
            {
                Response.Write("<script>alert('Publisher with such ID already exists.')</script>");
            }
            else
            {
                NewPublisher();
            }
        }

        protected void Update_Click(object sender, EventArgs e)
        {
            if (DoesPublisherExist())
            {
                UpdatePublisher();
            }
            else
            {
                Response.Write("<script>alert('Publisher with such ID does not exist.')</script>");
            }
        }

        protected void Delete_Click(object sender, EventArgs e)
        {
            if (DoesPublisherExist())
            {
                DeletePublisher();
            }
            else
            {
                Response.Write("<script>alert('Publisher with such ID does not exist.')</script>");
            }
        }

        bool DoesPublisherExist()
        {
            try
            {
                SqlConnection con = new SqlConnection(conn);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }

                SqlCommand selectQuery = new SqlCommand("SELECT * FROM [dbo].[Publisher] WHERE id = '" + Publisher_ID.Text.Trim() + "';", con);

                SqlDataAdapter da = new SqlDataAdapter(selectQuery);

                DataTable dt = new DataTable();

                da.Fill(dt);

                if (dt.Rows.Count >= 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception err)
            {
                Response.Write("<script>alert('" + err.Message + "')</script>");
                return false;
            }
        }

        void NewPublisher()
        {
            try
            {
                SqlConnection con = new SqlConnection(conn);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }

                SqlCommand insertQuery = new SqlCommand("INSERT INTO [dbo].[Publisher](id, name) VALUES(@id, @name)", con);

                insertQuery.Parameters.AddWithValue("@id", Publisher_ID.Text.Trim());
                insertQuery.Parameters.AddWithValue("@name", Publisher_Name.Text.Trim());

                insertQuery.ExecuteNonQuery();
                con.Close();
                Response.Write("<script>alert('New publisher has been successfully added!')</script>");
                ClearForm();
                Publishers.DataBind();
            }
            catch (Exception err)
            {
                Response.Write("<script>alert('" + err.Message + "')</script>");
            }
        }

        void UpdatePublisher()
        {
            try
            {
                SqlConnection con = new SqlConnection(conn);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }

                SqlCommand updateQuery = new SqlCommand("UPDATE [dbo].[Publisher] SET name = @name WHERE id = '" + Publisher_ID.Text.Trim() + "'", con);

                updateQuery.Parameters.AddWithValue("@name", Publisher_Name.Text.Trim());

                updateQuery.ExecuteNonQuery();
                con.Close();
                Response.Write("<script>alert('Publisher has been successfully updated!')</script>");
                ClearForm();
                Publishers.DataBind();
            }
            catch (Exception err)
            {
                Response.Write("<script>alert('" + err.Message + "')</script>");
            }
        }
        void DeletePublisher()
        {
            try
            {
                SqlConnection con = new SqlConnection(conn);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }

                SqlCommand deleteQuery = new SqlCommand("DELETE FROM [dbo].[Publisher] WHERE id = '" + Publisher_ID.Text.Trim() + "'", con);


                deleteQuery.ExecuteNonQuery();
                con.Close();
                Response.Write("<script>alert('Publisher has been successfully deleted!')</script>");
                ClearForm();
                Publishers.DataBind();
            }
            catch (Exception err)
            {
                Response.Write("<script>alert('" + err.Message + "')</script>");
            }
        }

        void ClearForm()
        {
            Publisher_ID.Text = "";
            Publisher_Name.Text = "";
        }

        void GetPublisherByID()
        {
            try
            {
                SqlConnection con = new SqlConnection(conn);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }

                SqlCommand selectQuery = new SqlCommand("SELECT * FROM [dbo].[Publisher] WHERE id = '" + Publisher_ID.Text.Trim() + "';", con);

                SqlDataAdapter da = new SqlDataAdapter(selectQuery);

                DataTable dt = new DataTable();

                da.Fill(dt);

                if (dt.Rows.Count >= 1)
                {
                    Publisher_Name.Text = dt.Rows[0][1].ToString();
                }
                else
                {
                    Response.Write("<script>alert('Publisher with such ID does not exist.')</script>");
                }

            }
            catch (Exception err)
            {
                Response.Write("<script>alert('" + err.Message + "')</script>");
            }
        }
    }
}