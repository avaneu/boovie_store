﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BoovieNested.master" AutoEventWireup="true" CodeBehind="blog_page_lion.aspx.cs" Inherits="BoovieStore.blog_page_lion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <main id="wrapper">
        <section class="jumbotron text-center">
            <div class="container">
                  <h2 class="jumbotron-heading mb-4">Blog</h2>
                  <p class="lead text-muted text-center">Read short descriptions of the books we offer.</p>
            </div>
      </section>

        <section>
            <div class="container">
                    <div class="row">
                        <div class="col-sm-8 blog-main">
                          <div class="blog-post">
                            <h2 class="blog-post-title">C.S. Lewis</h2>
                            <p class="blog-post-meta"><b>February 11, 2020 by <a href="#">Boovie</a></b></p>
                            <h3>Lion, the Witch and the Wardrobe</h3>
                            <p>Peter, Susan, Edmund, and Lucy Pevensie are four siblings sent to live in the country with the eccentric Professor Kirke during World War II. The children explore the house on a rainy day and Lucy, the youngest, finds an enormous wardrobe. Lucy steps inside and finds herself in a strange, snowy wood. Lucy encounters the Faun Tumnus, who is surprised to meet a human girl. Tumnus tells Lucy that she has entered Narnia, a different world. Tumnus invites Lucy to tea, and she accepts. Lucy and Tumnus have a wonderful tea, but the faun bursts into tears and confesses that he is a servant of the evil White Witch. The Witch has enchanted Narnia so that it is always winter and never Christmas. Tumnus explains that he has been enlisted to capture human beings. Lucy implores Tumnus to release her, and he agrees.</p>
                            <p>On the way back to the lamppost, the border between Narnia and our world, Edmund meets Lucy. Lucy tells Edmund about the White Witch. Edmund denies any connection between the Witch and the Queen. All Edmund can think about is his desire for the Turkish Delight. Lucy and Edmund return to Peter and Susan, back in their own world.</p>
                            <hr>
                        </div>
                    </div>

                    <div class="col-sm-3 offset-sm-1 blog-sidebar">
                        <div class="sidebar-module sidebar-module-inset">
                            <h4>Information</h4>
                            <p>These beautiful, instructive <em>books enter the complex worlds</em> of small living creatures and make fine introductions to natural science. They are profusely illustrated in full color and with scientific accuracy.</p>
                        </div>

                        <div class="col-md-12">
                            <img class="featurette-image img-fluid mx-auto" src="imgs/sidebar_lion.png" data-holder-rendered="true">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
</asp:Content>
