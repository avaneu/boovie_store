﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BoovieNested.Master" AutoEventWireup="true" CodeBehind="adminauthormanagement.aspx.cs" Inherits="BoovieStore.adminauthormanagement" %>

<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $(".table").prepend($("<thead></thead>").append($(this).find("tr:first"))).dataTable();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <main id="wrapper">
        <section class="has-margin-size-5">
            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <div class="element-center">
                                            <h4 class="d-block mx-auto">Author Details</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="element-center">
                                            <img width="100" src="imgs/nicolas.png"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <hr />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Author ID</label>
                                        <div class="mb-3">
                                            <div class="input-group">    
                                                <asp:TextBox class="form-control" ID="Author_ID" runat="server" placeholder="ID" TextMode="Number"></asp:TextBox>
                                                <asp:Button class="btn btn-primary" ID="Go" runat="server" Text="Go" OnClick="Go_Click"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <label>Author Name</label>
                                        <div class="mb-3">
                                            <asp:TextBox class="form-control" ID="Author_Name" runat="server" placeholder="Author Name"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-4 d-grid">
                                        <asp:Button class="btn w-100 btn-success btn-lg" ID="Add" runat="server" Text="Add" OnClick="Add_Click" />
                                    </div>  
                                    <div class="col-4 d-grid">
                                        <asp:Button class="btn w-100 btn-primary btn-lg" ID="Update" runat="server" Text="Update" OnClick="Update_Click" />
                                    </div>  
                                    <div class="col-4 d-grid">
                                        <asp:Button class="btn w-100 btn-danger btn-lg" ID="Delete" runat="server" Text="Delete" OnClick="Delete_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="homepage.aspx" class="btn-back"><i class="fas fa-long-arrow-alt-left"></i> Back to Homepage</a>
                    </div>            
                    <div class="col-md-7">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <div class="element-center">
                                            <h4>Author List</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <hr />
                                    </div>
                                </div>
                                <div class="row">
                                    <asp:SqlDataSource ID="Authors_Source" runat="server" ConnectionString="<%$ ConnectionStrings:BoovieStoreConnectionString %>" SelectCommand="SELECT * FROM [Author]"></asp:SqlDataSource>
                                    <div class="col">
                                        <asp:GridView class="table table-striped table-bordered hover cell-border" ID="Authors" runat="server" AutoGenerateColumns="False" DataKeyNames="id" DataSourceID="Authors_Source">
                                            <Columns>
                                                <asp:BoundField DataField="id" HeaderText="Author ID" ReadOnly="True" SortExpression="id" />
                                                <asp:BoundField DataField="name" HeaderText="Author Name" SortExpression="name" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
</asp:Content>
