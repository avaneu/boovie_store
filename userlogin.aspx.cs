﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BoovieStore
{
    public partial class userlogin : System.Web.UI.Page
    {
        string conn = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            string sessionName = Session["name"] as string;
            string sessionRole = Session["role"] as string;
            if(!string.IsNullOrEmpty(sessionName)) {
                Response.Redirect("userprofile.aspx");
            }
        }

        protected void Login_Click(object sender, EventArgs e)
        {
            try {
                SqlConnection con = new SqlConnection(conn);
                if (con.State == ConnectionState.Closed) {
                    con.Open();
                }

                SqlCommand selectQuery = new SqlCommand("SELECT * FROM [dbo].[User] WHERE id = '" + User_ID.Text.Trim() + "' AND password = '" + Password.Text.Trim() + "';", con);
                SqlDataAdapter da = new SqlDataAdapter(selectQuery);
                DataTable dt = new DataTable();
                da.Fill(dt);

                if (dt.Rows.Count >= 1) {
                    Response.Write("<script>alert('Login successful. Redirecting to homepage');</script>");
                    Session["id"] = dt.Rows[0][0];
                    Session["name"] = dt.Rows[0][1];
                    Session["status"] = dt.Rows[0][9];
                    Session["role"] = "user";
                    Response.Write("<script>alert('" + Session["name"] + "')</script>");
                    Response.Redirect("homepage.aspx");
                }
                else {
                    Response.Write("<script>alert('Invalid login credentials.');</script>");
                }                                       
            }                                                           
            catch (Exception err) {                                     
                Response.Write("<script>alert('" + err.Message + "');</script>");
            }                                                           
        }                                                               
    }                                                                   
}                                                                       