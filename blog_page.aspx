﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BoovieNested.master" AutoEventWireup="true" CodeBehind="blog_page.aspx.cs" Inherits="BoovieStore.blog_page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<main id="wrapper">
    <section class="jumbotron text-center">
        <div class="container">
                <h2 class="jumbotron-heading mb-4">Blog</h2>
                <p class="lead text-muted text-center">Read short descriptions of the books we offer.</p>
        </div>
    </section>

    <section>
        <div class="container">
                <div class="row">
                    <div class="col-sm-8 blog-main">
                        <div class="blog-post">
                        <h2 class="blog-post-title">J.K.Rowling</h2>
                        <p class="blog-post-meta"><b>February 10, 2020 by <a href="#">Boovie</a></b></p>
                        <h3>Harry Potter and the Philosopher's Stone</h3>
                        <p>Harry Potter and the Philosopher's Stone is a fantasy novel written by British author J. K. Rowling. The first novel in the Harry Potter series and Rowling's debut novel, it follows Harry Potter, a young wizard who discovers his magical heritage on his eleventh birthday, when he receives a letter of acceptance to Hogwarts School of Witchcraft and Wizardry. Harry makes close friends and a few enemies during his first year at the school, and with the help of his friends, Harry faces an attempted comeback by the dark wizard Lord Voldemort, who killed Harry's parents, but failed to kill Harry when he was just 15 months old.</p>
                        <p>The book was first published in the United Kingdom on 26 June 1997 by Bloomsbury. It was published in the United States the following year by Scholastic Corporation under the title Harry Potter and the Sorcerer's Stone. It won most of the British book awards that were judged by children and other awards in the US. The book reached the top of the New York Times list of best-selling fiction in August 1999 and stayed near the top of that list for much of 1999 and 2000. It has been translated into at least 73 other languages, and has been made into a feature-length film of the same name, as have all six of its sequels. The novel has sold in excess of 120 million copies.</p>
                        <hr>
                    </div>
                </div>

                <div class="col-sm-3 offset-sm-1 blog-sidebar">
                    <div class="sidebar-module sidebar-module-inset">
                        <h4>Information</h4>
                        <p>These beautiful, instructive <em>books enter the complex worlds</em> of small living creatures and make fine introductions to natural science. They are profusely illustrated in full color and with scientific accuracy.</p>
                    </div>

                    <div class="col-md-12">
                        <img class="featurette-image img-fluid mx-auto" src="imgs/sidebar.png" data-holder-rendered="true">
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
</asp:Content>
