﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BoovieNested.master" AutoEventWireup="true" CodeBehind="blog_page_witcher.aspx.cs" Inherits="BoovieStore.blog_page_witcher" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <main id="wrapper">
       <section class="jumbotron text-center">
            <div class="container">
                  <h2 class="jumbotron-heading mb-4">Blog</h2>
                  <p class="lead text-muted text-center">Read short descriptions of the books we offer.</p>
            </div>
      </section>

      <section>
            <div class="container">
                    <div class="row">
                        <div class="col-sm-8 blog-main">
                            <div class="blog-post">
                            <h2 class="blog-post-title">Andrzej Sapkowski</h2>
                            <p class="blog-post-meta"><b>February 12, 2020 by <a href="#">Boovie</a></b></p>
                            <h3>The Witcher: The Last Wish</h3>
                            <p>The Last Wish (Polish: Ostatnie życzenie) is the first book in Andrzej Sapkowski's The Witcher series in terms of story chronology, although the original Polish edition was published in 1993, after Sword of Destiny. The King of Temeria, Foltest, has offered a reward to anyone who can lift the curse on his daughter, Adda (the result of an incestuous union with his late sister, also named Adda), who was born as a striga, and now terrorizes the town every night. Foltest insists that his daughter not be harmed, but reluctantly grants Geralt permission to kill her if Geralt decides that Adda cannot be returned to human form. A more difficult question, to which Geralt can give no certain answer, is whether Adda, who has been a monster her whole life, can live as a "normal" human even if the curse is lifted. </p>
                            <p>Some of the individual short stories were first published in the Fantastyka magazine or in the Wiedźmin short story collection (the first collection of Sapkowski's stories, out of print and now obsolete; all short stories were later collected in The Last Wish, Sword of Destiny and Something Ends, Something Begins) and as such were the first witcher stories published.</p>
                            <hr>
                        </div>
                    </div>

                    <div class="col-sm-3 offset-sm-1 blog-sidebar">
                        <div class="sidebar-module sidebar-module-inset">
                            <h4>Information</h4>
                            <p>These beautiful, instructive <em>books enter the complex worlds</em> of small living creatures and make fine introductions to natural science. They are profusely illustrated in full color and with scientific accuracy.</p>
                        </div>

                        <div class="col-md-12">
                            <img class="featurette-image img-fluid mx-auto" src="imgs/sidebar_witcher.png" data-holder-rendered="true">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
</asp:Content>
