﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BoovieStore
{
    public partial class signup : System.Web.UI.Page
    {
        string conn = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Sign_Up_Click(object sender, EventArgs e)
        {
            if(DoesUserExists())
            {
                Response.Write("<script>alert('User with this ID already exists.')</script>");
            }
            else
            {
                NewUser();
            }
        }

        bool DoesUserExists()
        {
            try
            {
                SqlConnection con = new SqlConnection(conn);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }

                SqlCommand selectQuery = new SqlCommand("SELECT * FROM [dbo].[User] WHERE id = '" + User_ID.Text.Trim() + "';", con);

                SqlDataAdapter da = new SqlDataAdapter(selectQuery);

                DataTable dt = new DataTable();

                da.Fill(dt);

                if (dt.Rows.Count >= 1) {
                    return true;
                } else {
                    return false;
                }

            }
            catch (Exception err)
            {
                Response.Write("<script>alert('" + err.Message + "')</script>");
                return false;
            }
        }

        void NewUser()
        {
            try
            {
                SqlConnection con = new SqlConnection(conn);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }

                SqlCommand insertQuery = new SqlCommand("INSERT INTO [dbo].[User](id, name, password, email, date_of_birth, contact_no, state, city, full_address, account_status) VALUES(@id, @name, @password, @email, @date_of_birth, @contact_no, @state, @city, @full_address, @account_status)", con);

                insertQuery.Parameters.AddWithValue("@id", User_ID.Text.Trim());
                insertQuery.Parameters.AddWithValue("@name", Full_Name.Text.Trim());
                insertQuery.Parameters.AddWithValue("@password", Password.Text.Trim());
                insertQuery.Parameters.AddWithValue("@email", Email_Address.Text.Trim());
                insertQuery.Parameters.AddWithValue("@date_of_birth", Date_Of_Birth.Text.Trim());
                insertQuery.Parameters.AddWithValue("@contact_no", Contact_No.Text.Trim());
                insertQuery.Parameters.AddWithValue("@state", State.SelectedItem.Value);
                insertQuery.Parameters.AddWithValue("@city", City.Text.Trim());
                insertQuery.Parameters.AddWithValue("@full_address", Full_Address.Text.Trim());
                insertQuery.Parameters.AddWithValue("@account_status", "pending");

                insertQuery.ExecuteNonQuery();
                con.Close();
                Response.Write("<script>alert('You have been successfully registered. Please wait for account approval by an Administrator.')</script>");
            }
            catch (Exception err)
            {
                Response.Write("<script>alert('" + err.Message + "')</script>");
            }
        }
    }
}