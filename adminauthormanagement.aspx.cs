﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BoovieStore
{
    public partial class adminauthormanagement : System.Web.UI.Page
    {
        string conn = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            string sessionName = Session["name"] as string;
            string sessionRole = Session["role"] as string;
            if (string.IsNullOrEmpty(sessionName))
            {
                Response.Write("<script>alert('You are not logged in. Please login again to continue.');</script>");
                Response.Redirect("userlogin.aspx");
            }
            else if (sessionRole == "user") {
                Response.Redirect("userprofile.aspx");
            }
            Authors.DataBind();
        }

        protected void Go_Click(object sender, EventArgs e)
        {
            GetAuthorByID();
        }
        protected void Add_Click(object sender, EventArgs e)
        {
            if(DoesAuthorExist()) {
                Response.Write("<script>alert('Author with such ID already exists.')</script>");
            }
            else {
                NewAuthor();
            }
        }
        protected void Update_Click(object sender, EventArgs e)
        {
            if (DoesAuthorExist()) {
                UpdateAuthor();
            }
            else {
                Response.Write("<script>alert('Author with such ID does not exist.')</script>");
            }
        }
        protected void Delete_Click(object sender, EventArgs e)
        {
            if (DoesAuthorExist())
            {
                DeleteAuthor();
            }
            else
            {
                Response.Write("<script>alert('Author with such ID does not exist.')</script>");
            }
        }
        bool DoesAuthorExist()
        {
            try
            {
                SqlConnection con = new SqlConnection(conn);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }

                SqlCommand selectQuery = new SqlCommand("SELECT * FROM [dbo].[Author] WHERE id = '" + Author_ID.Text.Trim() + "';", con);

                SqlDataAdapter da = new SqlDataAdapter(selectQuery);

                DataTable dt = new DataTable();

                da.Fill(dt);

                if (dt.Rows.Count >= 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception err)
            {
                Response.Write("<script>alert('" + err.Message + "')</script>");
                return false;
            }
        }

        void NewAuthor() {
            try
            {
                SqlConnection con = new SqlConnection(conn);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }

                SqlCommand insertQuery = new SqlCommand("INSERT INTO [dbo].[Author](id, name) VALUES(@id, @name)", con);

                insertQuery.Parameters.AddWithValue("@id", Author_ID.Text.Trim());
                insertQuery.Parameters.AddWithValue("@name", Author_Name.Text.Trim());

                insertQuery.ExecuteNonQuery();
                con.Close();
                Response.Write("<script>alert('New author has been successfully added!')</script>");
                ClearForm();
                Authors.DataBind();
            }
            catch (Exception err)
            {
                Response.Write("<script>alert('" + err.Message + "')</script>");
            }
        }

        void UpdateAuthor() {
            try
            {
                SqlConnection con = new SqlConnection(conn);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }

                SqlCommand updateQuery = new SqlCommand("UPDATE [dbo].[Author] SET name = @name WHERE id = '" + Author_ID.Text.Trim() + "'", con);

                updateQuery.Parameters.AddWithValue("@name", Author_Name.Text.Trim());

                updateQuery.ExecuteNonQuery();
                con.Close();
                Response.Write("<script>alert('Author has been successfully updated!')</script>");
                ClearForm();
                Authors.DataBind();
            }
            catch (Exception err)
            {
                Response.Write("<script>alert('" + err.Message + "')</script>");
            }
        }
        void DeleteAuthor() {
            try
            {
                SqlConnection con = new SqlConnection(conn);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }

                SqlCommand deleteQuery = new SqlCommand("DELETE FROM [dbo].[Author] WHERE id = '" + Author_ID.Text.Trim() + "'", con);


                deleteQuery.ExecuteNonQuery();
                con.Close();
                Response.Write("<script>alert('Author has been successfully deleted!')</script>");
                ClearForm();
                Authors.DataBind();
            }
            catch (Exception err)
            {
                Response.Write("<script>alert('" + err.Message + "')</script>");
            }
        }

        void ClearForm() {
            Author_ID.Text = "";
            Author_Name.Text = "";
        }

        void GetAuthorByID() {
            try
            {
                SqlConnection con = new SqlConnection(conn);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }

                SqlCommand selectQuery = new SqlCommand("SELECT * FROM [dbo].[Author] WHERE id = '" + Author_ID.Text.Trim() + "';", con);

                SqlDataAdapter da = new SqlDataAdapter(selectQuery);

                DataTable dt = new DataTable();

                da.Fill(dt);

                if (dt.Rows.Count >= 1) {
                    Author_Name.Text = dt.Rows[0][1].ToString();
                }
                else {
                    Response.Write("<script>alert('Author with such ID does not exist.')</script>");
                }

            }
            catch (Exception err)
            {
                Response.Write("<script>alert('" + err.Message + "')</script>");
            }
        }
    }
}