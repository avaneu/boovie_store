﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BoovieNested.Master" AutoEventWireup="true" CodeBehind="signup.aspx.cs" Inherits="BoovieStore.signup" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <main id="wrapper">
        <section class="has-margin-size-5">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 mx-auto">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <div class="element-center">
                                            <img width="100" src="imgs/nicolas.png"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="element-center">
                                            <h4>User Registration</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <hr />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Full name</label>
                                        <div class="mb-3">
                                            <asp:TextBox class="form-control" ID="Full_Name" runat="server" placeholder="Full name"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Date of birth</label>
                                        <div class="mb-3">
                                            <asp:TextBox class="form-control" ID="Date_Of_Birth" runat="server" placeholder="Date of birth" TextMode="Date"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Contact No.</label>
                                        <div class="mb-3">
                                            <asp:TextBox class="form-control" ID="Contact_No" runat="server" placeholder="Contact No." TextMode="Number"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Email Address</label>
                                        <div class="mb-3">
                                            <asp:TextBox class="form-control" ID="Email_Address" runat="server" placeholder="Email Address" TextMode="Email"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>State</label>
                                        <div class="mb-3">
                                            <asp:DropDownList class="form-control" ID="State" runat="server">
                                                <asp:ListItem Text="Florida" Value="Florida" />
                                                <asp:ListItem Text="Washington DC" Value="Washington DC" />
                                                <asp:ListItem Text="Indiana Polis" Value="Indiana Polis" />
                                                <asp:ListItem Text="Ohio" Value="Ohio" />
                                                <asp:ListItem Text="Georgia" Value="Georgia" />
                                                <asp:ListItem Text="Texas" Value="Texas" />
                                                <asp:ListItem Text="Arizona" Value="Arizona" />
                                                <asp:ListItem Text="Michigan" Value="Michigan" />
                                                <asp:ListItem Text="Colorado" Value="Colorado" />
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>City</label>
                                        <div class="mb-3">
                                            <asp:TextBox class="form-control" ID="City" runat="server" placeholder="City"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label>Full address</label>
                                        <div class="mb-3">
                                            <asp:TextBox class="form-control" ID="Full_Address" runat="server" placeholder="Full Address" TextMode="MultiLine" Rows="2"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="element-center">
                                            <span class="badge rounded-pill bg-info">Login credentials</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>User ID</label>
                                        <div class="mb-3">
                                            <asp:TextBox class="form-control" ID="User_ID" runat="server" placeholder="User ID"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Password</label>
                                        <div class="mb-3">
                                            <asp:TextBox class="form-control" ID="Password" runat="server" placeholder="Password" TextMode="Password"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-8 mx-auto">
                                        <div class="element-center">
                                            <div class="mb-3 d-grid">
                                                <asp:Button class="btn btn-info btn-lg w-100" ID="Sign_Up" runat="server" Text="Sign Up" OnClick="Sign_Up_Click" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="homepage.aspx" class="btn-back featurette-divider-bottom mt-4"><i class="fas fa-long-arrow-alt-left"></i> Back to Homepage</a>
                    </div>            
                </div>
            </div>
        </section>
    </main>
</asp:Content>
