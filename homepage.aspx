﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BoovieNested.Master" AutoEventWireup="true" CodeBehind="homepage.aspx.cs" Inherits="BoovieStore.homepage" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <main id="wrapper">
        <section>
            <div id="demo" class="carousel slide" data-ride="carousel">
                <ul class="carousel-indicators">
                    <li data-target="#demo" data-slide-to="0" class="active"></li>
                    <li data-target="#demo" data-slide-to="1"></li>
                    <li data-target="#demo" data-slide-to="2"></li>
                </ul>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="carousel-images" src="imgs/1.jpg" alt="book">
                        <div class="carousel-caption">
                            <h3 class="carousel-header">Andrzej Sapkowski "Sword of Destiny"</h3>
                            <p class="carousel-paragraph">“There is never a second opportunity to make a first impression.” </p>
                        </div>   
                    </div>
                    <div class="carousel-item">
                        <img class="carousel-images" src="imgs/2.jpg" alt="book">
                        <div class="carousel-caption">
                            <h3 class="carousel-header">George R.R.Martin "Game of Throne"</h3>
                            <p class="carousel-paragraph">"Valar Morghulis"</p>
                        </div>   
                    </div>
                    <div class="carousel-item">
                        <img class="carousel-images" src="imgs/3.jpg" alt="book">
                        <div class="carousel-caption">
                            <h3 class="carousel-header">J.K Rowling "Harry potter and Philosopher's Stone"</h3>
                            <p class="carousel-paragraph">"Yer a wizard Harry."[...]"Dobby is free."</p>
                        </div>   
                    </div>
                </div>
                <a class="carousel-control-prev" href="#demo" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                </a>
                <a class="carousel-control-next" href="#demo" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                </a>
            </div>
        </section>

        <section>
            <div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center bg-light">
                <div class="col-md-5 mx-auto">
                    <h1 class="display-4 font-weight-normal mt-4 mb-4">BoovieStore</h1>
                    <p class="lead mt-4 mb-4">Enter the world of books with us and experience many unknown adventures.</p>
                    <a class="btn btn-black-style mt-4 mb-4" href="aboutus.aspx">About Us</a>
                </div>

            <div class="product-device box-shadow d-none d-md-block"></div>

            <div class="product-device product-device-2 box-shadow d-none d-md-block"></div>

            </div>
        </section>

        <section>

            <div class="album py-1">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card mb-4 shadow-sm">
                                <img class="bd-placeholder-img card-img-top" src="imgs/book1.jpg"/>
                                <div class="card-body">
                                    <p class="card-text">Harry Potter is a series of seven fantasy novels written by British author J. K. Rowling...</p>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <div class="btn-group">
                                            <div class="btn btn-sm btn-outline-secondary btn-white-style"><a href="blog_page.aspx">View</a></div>
                                        </div>
                                        <small class="text-muted">3 mins</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card mb-4 shadow-sm">
                                <img class="bd-placeholder-img card-img-top" src="imgs/the_witcher_tlw.jpg"/>
                                <div class="card-body">
                                    <p class="card-text">The Last Wish (Polish: Ostatnie życzenie) is the first book in Andrzej Sapkowski's The Witcher series in terms of story...</p>
                                    <div class="d-flex justify-content-between align-items-center">
                                    <div class="btn-group">
                                        <div class="btn btn-sm btn-outline-secondary btn-white-style"><a href="blog_page_witcher.aspx">View</a></div>
                                    </div>
                                    <small class="text-muted">5 mins</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card mb-4 shadow-sm">
                                <img class="bd-placeholder-img card-img-top" src="imgs/Wardrobe.png"/>
                                <div class="card-body">
                                    <p class="card-text">Peter, Susan, Edmund, and Lucy Pevensie are four siblings sent to live in the country with the eccentric...</p>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <div class="btn-group">
                                            <div class="btn btn-sm btn-outline-secondary btn-white-style"><a href="blog_page_lion.aspx">View</a></div>
                                        </div>
                                        <small class="text-muted">4 mins</small>
                                    </div>
                                </div>
                            </div>
                        </div>      
                    </div>
                </div>
            </div>
        </section>
    </main>
</asp:Content>
