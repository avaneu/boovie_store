﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BoovieNested.Master" AutoEventWireup="true" CodeBehind="userprofile.aspx.cs" Inherits="BoovieStore.userprofile" %>

<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $(".table").prepend($("<thead></thead>").append($(this).find("tr:first"))).dataTable();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <main id="wrapper">
        <section class="has-margin-size-5">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-5">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <div class="element-center">
                                            <img width="100" src="imgs/nicolas.png"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="element-center">
                                            <h4>Your profile</h4>
                                            <span>Account status -</span>
                                            <asp:Label class="badge bg-info rounded-pill" ID="Status" runat="server" Text="Your status"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <hr />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Full name</label>
                                        <div class="mb-3">
                                            <asp:TextBox class="form-control" ID="Full_Name" runat="server" placeholder="Full name"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Date of birth</label>
                                        <div class="mb-3">
                                            <asp:TextBox class="form-control" ID="Date_Of_Birth" runat="server" placeholder="Date of birth" TextMode="Date"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Contact No.</label>
                                        <div class="mb-3">
                                            <asp:TextBox class="form-control" ID="Contact_No" runat="server" placeholder="Contact No." TextMode="Number"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Email Address</label>
                                        <div class="mb-3">
                                            <asp:TextBox class="form-control" ID="Email_Address" runat="server" placeholder="Email Address" TextMode="Email"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>State</label>
                                        <div class="mb-3">
                                            <asp:DropDownList class="form-control" ID="States" runat="server">
                                                <asp:ListItem Text="Florida" Value="Florida" />
                                                <asp:ListItem Text="Washington DC" Value="Washington DC" />
                                                <asp:ListItem Text="Indiana Polis" Value="Indiana Polis" />
                                                <asp:ListItem Text="Ohio" Value="Ohio" />
                                                <asp:ListItem Text="Georgia" Value="Georgia" />
                                                <asp:ListItem Text="Texas" Value="Texas" />
                                                <asp:ListItem Text="Arizona" Value="Arizona" />
                                                <asp:ListItem Text="Michigan" Value="Michigan" />
                                                <asp:ListItem Text="Colorado" Value="Colorado" />
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>City</label>
                                        <div class="mb-3">
                                            <asp:TextBox class="form-control" ID="City" runat="server" placeholder="City"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label>Full address</label>
                                        <div class="mb-3">
                                            <asp:TextBox class="form-control" ID="Full_Address" runat="server" placeholder="Full address" TextMode="MultiLine" Rows="2"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="element-center">
                                            <span class="badge rounded-pill bg-info">Login credentials</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>User ID</label>
                                        <div class="mb-3">
                                            <asp:TextBox class="form-control" ID="User_ID" runat="server" placeholder="User ID" ReadOnly="True"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Old password</label>
                                        <div class="mb-3">
                                            <asp:TextBox class="form-control" ID="Old_Password" runat="server" placeholder="Old password" ReadOnly="True"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label>New password</label>
                                        <div class="mb-3">
                                            <asp:TextBox class="form-control" ID="New_Password" runat="server" placeholder="New password" TextMode="Password"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-8 mx-auto">
                                        <div class="element-center">
                                            <div class="mb-3 d-grid">
                                                <asp:Button class="btn btn-primary btn-lg w-100" ID="Update" runat="server" Text="Update" OnClick="Update_Click" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="homepage.aspx" class="btn-back mt-4"><i class="fas fa-long-arrow-alt-left"></i> Back to Homepage</a>
                    </div>            
                    <div class="col-md-7">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <div class="element-center"><img width="100" src="imgs/books.png"/></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="element-center">
                                            <h4>Your issued books</h4>
                                            <asp:Label class="badge bg-info rounded-pill" ID="Books_info" runat="server" Text="Your books info"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <hr />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:GridView class="table table-striped table-bordered hover cell-border" ID="Issued" runat="server" OnRowDataBound="Issued_RowDataBound"></asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
</asp:Content>
