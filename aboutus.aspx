﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BoovieNested.Master" AutoEventWireup="true" CodeBehind="aboutus.aspx.cs" Inherits="BoovieStore.aboutus" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <main id="wrapper">
        <section class="bgimage">
          <div class="container-fluid">
            <div class="row">
              <div>
                  <div class="hero-text">
                    <h2 class="mb-4">About Us</h2>
                    <p>We are a company that sells both books and movies. You can find specific descriptions on our website. Experience an adventure with us in the fantasy world today!</p>
                  </div>
              </div>
            </div>
          </div>
        </section>   
    
        <section>
            <div class="container">
                <div class="featurette-divider row featurette">
                      <div class="col-md-6">
                        <h2 class="featurette-heading">About Us</h2>
                        <p><span class="text-muted">A few words about our business</span></p>
                        <p class="lead lead-justify">Our company was founded in 2015. For many years, it has been offering selected activities from various departments. Starting with history books and ending with fantasy. We offer both books and films. What distinguishes us from our competitors? First of all, a selection of selected works and discovering the latest novelties in front of others. Then choosing the right cover that will attract the viewer.</p>
                      </div>
                      <div class="col-md-6">
                        <img class="featurette-image img-fluid mx-auto" data-src="holder.js/500x500/auto" alt="500x500" style="width: 500px; height: 500px;" src="imgs/about-1.png" data-holder-rendered="true">
                      </div>
                </div>
            </div>
        </section>

        <section>
            <div class="container">
                <div class="featurette-divider row featurette">
                    <div class="jumbotron featurette-divider-bottom">
                        <h2 class="jumbotron-header">Discover what our store offers</h2>
                        <p class="jumbotron-paragraph lead">Click the button below - find the work or movie that you like. Contact us. We are happy to advise you!</p>
                        <a class="btn btn-jumbotron-style mt-4" href="" role="button">Click here</a>
                    </div>
                </div>
            </div>
        </section>
</main>
</asp:Content>
