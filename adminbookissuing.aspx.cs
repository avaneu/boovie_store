﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BoovieStore
{
    public partial class adminbookissuing : System.Web.UI.Page
    {
        string conn = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            string sessionName = Session["name"] as string;
            string sessionRole = Session["role"] as string;
            if (string.IsNullOrEmpty(sessionName))
            {
                Response.Write("<script>alert('You are not logged in. Please login again to continue.');</script>");
                Response.Redirect("userlogin.aspx");
            }
            else if (sessionRole == "user") {
                Response.Redirect("userprofile.aspx");
            }
            Issued.DataBind();
        }

        protected void Go_Click(object sender, EventArgs e)
        {
            GetBookAndUser();
        }

        protected void Issue_Click(object sender, EventArgs e)
        {
            if(DoesBookExist() && DoesUserExist()) {
                if(DoesEntryExist()) {
                    Response.Write("<script>alert('User cannot issue two identical books.');</script>");
                }
                else {
                    IssueBook();
                }
            }
            else {
                Response.Write("<script>alert('User and/or book with such IDs does not exist!');</script>");
            }
        }

        protected void Return_Click(object sender, EventArgs e)
        {
            if (DoesBookExist() && DoesUserExist())
            {
                if (DoesEntryExist())
                {
                    ReturnBook();
                }
                else
                {
                    Response.Write("<script>alert('User with such ID did not issue a book with matching ID');</script>");
                }
            }
            else
            {
                Response.Write("<script>alert('User and/or book with such IDs does not exist!');</script>");
            }
        }

        void GetBookAndUser()
        {
            try {
                SqlConnection con = new SqlConnection(conn);
                if(con.State == ConnectionState.Closed) {
                    con.Open();
                }

                SqlCommand selectQuery1 = new SqlCommand("SELECT name FROM [dbo].[Book] WHERE id = '" + Book_ID.Text.Trim() + "'", con);
                SqlDataAdapter da1 = new SqlDataAdapter(selectQuery1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);

                if(dt1.Rows.Count >= 1) {
                    Book_Title.Text = dt1.Rows[0][0].ToString();
                }
                else {
                    Response.Write("<script>alert('Book with such ID does not exist!');</script>");
                }

                SqlCommand selectQuery2 = new SqlCommand("SELECT name FROM [dbo].[User] WHERE id = '" + User_ID.Text.Trim() + "'", con);
                SqlDataAdapter da2 = new SqlDataAdapter(selectQuery2);
                DataTable dt2 = new DataTable();
                da2.Fill(dt2);

                if (dt2.Rows.Count >= 1) {
                    User_Name.Text = dt2.Rows[0][0].ToString();
                }
                else {
                    Response.Write("<script>alert('User with such ID does not exist!');</script>");
                }
            }
            catch(Exception err) {
                Response.Write("<script>alert('" + err.Message + "');</script>");
            }
        }

        bool DoesBookExist()
        {
            try
            {
                SqlConnection con = new SqlConnection(conn);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }

                SqlCommand selectQuery = new SqlCommand("SELECT * FROM [dbo].[Book] WHERE id = '" + Book_ID.Text.Trim() + "' AND current_stock > 0", con);
                SqlDataAdapter da = new SqlDataAdapter(selectQuery);
                DataTable dt = new DataTable();
                da.Fill(dt);

                if (dt.Rows.Count >= 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception err)
            {
                Response.Write("<script>alert('" + err.Message + "')</script>");
                return false;
            }
        }


        bool DoesUserExist()
        {
            try {
                SqlConnection con = new SqlConnection(conn);
                if (con.State == ConnectionState.Closed) {
                    con.Open();
                }

                SqlCommand selectQuery = new SqlCommand("SELECT * FROM [dbo].[User] WHERE id = '" + User_ID.Text.Trim() + "';", con);
                SqlDataAdapter da = new SqlDataAdapter(selectQuery);
                DataTable dt = new DataTable();
                da.Fill(dt);

                if (dt.Rows.Count >= 1) {
                    return true;
                }
                else {
                    return false;
                }

            }
            catch (Exception err) {
                Response.Write("<script>alert('" + err.Message + "')</script>");
                return false;
            }
        }

        void IssueBook()
        {
            try {
                SqlConnection con = new SqlConnection(conn);
                if (con.State == ConnectionState.Closed) {
                    con.Open();
                }

                SqlCommand insertQuery = new SqlCommand("INSERT INTO [dbo].[BookIssuing](user_id, user_name, book_id, book_name, issue_date, due_date) VALUES(@user_id, @user_name, @book_id, @book_name, @issue_date, @due_date)", con);

                insertQuery.Parameters.AddWithValue("@user_id", User_ID.Text.Trim());
                insertQuery.Parameters.AddWithValue("@user_name", User_Name.Text.Trim());
                insertQuery.Parameters.AddWithValue("@book_id", Book_ID.Text.Trim());
                insertQuery.Parameters.AddWithValue("@book_name", Book_Title.Text.Trim());
                insertQuery.Parameters.AddWithValue("@issue_date", Issue_Date.Text.Trim());
                insertQuery.Parameters.AddWithValue("@due_date", Due_Date.Text.Trim());

                insertQuery.ExecuteNonQuery();

                insertQuery = new SqlCommand("UPDATE [dbo].[Book] SET current_stock = current_stock-1 WHERE id = '" + Book_ID.Text.Trim() + "'", con);
                insertQuery.ExecuteNonQuery();

                con.Close();
                Response.Write("<script>alert('Book has been successfully issued!')</script>");
                Issued.DataBind();
            }
            catch (Exception err) {
                Response.Write("<script>alert('" + err.Message + "')</script>");
            }
        }

        bool DoesEntryExist()
        {
            try
            {
                SqlConnection con = new SqlConnection(conn);
                if (con.State == ConnectionState.Closed) {
                    con.Open();
                }

                SqlCommand selectQuery = new SqlCommand("SELECT * FROM [dbo].[BookIssuing] WHERE user_id = '" + User_ID.Text.Trim() + "' AND book_id = '" + Book_ID.Text.Trim() + "'", con);
                SqlDataAdapter da = new SqlDataAdapter(selectQuery);
                DataTable dt = new DataTable();
                da.Fill(dt);

                if (dt.Rows.Count >= 1) {
                    return true;
                }
                else {
                    return false;
                }

            }
            catch (Exception err)
            {
                Response.Write("<script>alert('" + err.Message + "')</script>");
                return false;
            }
        }
        void ReturnBook()
        {
            try
            {
                SqlConnection con = new SqlConnection(conn);
                if (con.State == ConnectionState.Closed) {
                    con.Open();
                }

                SqlCommand deleteQuery = new SqlCommand("DELETE FROM [dbo].[BookIssuing] WHERE user_id = '" + User_ID.Text.Trim() + "' AND book_id = '" + Book_ID.Text.Trim() + "'", con);
                int res = deleteQuery.ExecuteNonQuery();

                if(res > 0) {
                    deleteQuery = new SqlCommand("UPDATE [dbo].[Book] SET current_stock = current_stock+1 WHERE id = '" + Book_ID.Text.Trim() + "'", con);
                    deleteQuery.ExecuteNonQuery();

                    Response.Write("<script>alert('Book has been successfully returned!')</script>");
                    Issued.DataBind();
                    con.Close();
                }
                else {
                    Response.Write("<script>alert('Given information have produced an error. Try again.')</script>");
                }

            }
            catch (Exception err) {
                Response.Write("<script>alert('" + err.Message + "')</script>");
            }
        }

        protected void Issued_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try {
                if(e.Row.RowType == DataControlRowType.DataRow) {
                    DateTime due = Convert.ToDateTime(e.Row.Cells[5].Text);
                    DateTime today = DateTime.Today;
                    if(today > due) {
                        e.Row.BackColor = System.Drawing.Color.DeepPink;
                    }
                }
            }
            catch(Exception err) {
                Response.Write("<script>alert('" + err.Message + "')</script>");
            }
        }
    }
}