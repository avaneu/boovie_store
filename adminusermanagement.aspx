﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BoovieNested.Master" AutoEventWireup="true" CodeBehind="adminusermanagement.aspx.cs" Inherits="BoovieStore.adminusermanagement" %>

<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $(".table").prepend($("<thead></thead>").append($(this).find("tr:first"))).dataTable();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <main id="wrapper">
        <section class="has-margin-size-5">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-5">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <div class="element-center">
                                            <h4 class="d-block mx-auto">User Details</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="element-center">
                                            <img width="100" src="imgs/nicolas.png"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <hr />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>User ID</label>
                                        <div class="mb-3">
                                            <div class="input-group">    
                                                <asp:TextBox class="form-control" ID="User_ID" runat="server" placeholder="ID"></asp:TextBox>
                                                <asp:LinkButton class="btn btn-primary" ID="Go" runat="server" Text="" OnClick="Go_Click">
                                                    <i class="fas fa-check-circle"></i>
                                                </asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Full Name</label>
                                        <div class="mb-3">
                                            <asp:TextBox class="form-control" ID="Full_Name" runat="server" placeholder="Full Name" ReadOnly="True"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <label>Account Status</label>
                                        <div class="mb-3">
                                            <div class="input-group">    
                                                <asp:TextBox class="form-control me-1" ID="Account_Status" runat="server" placeholder="Status" ReadOnly="True"></asp:TextBox>
                                                <asp:LinkButton class="btn btn-success me-1" ID="Activate" runat="server" Text="" OnClick="Activate_Click">
                                                    <i class="fas fa-check-circle"></i>
                                                </asp:LinkButton>
                                                <asp:LinkButton class="btn btn-warning me-1" ID="Suspend" runat="server" Text="" OnClick="Suspend_Click">
                                                    <i class="fas fa-pause-circle"></i>
                                                </asp:LinkButton>
                                                <asp:LinkButton class="btn btn-danger" ID="Deactivate" runat="server" Text="" OnClick="Deactivate_Click">
                                                    <i class="fas fa-times-circle"></i>
                                                </asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>Date of Birth</label>
                                        <div class="mb-3">
                                            <asp:TextBox class="form-control" ID="Date_Of_Birth" runat="server" placeholder="Date of Birth" ReadOnly="True"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Contact No.</label>
                                        <div class="mb-3">
                                            <asp:TextBox class="form-control" ID="Contact_No" runat="server" placeholder="Contact No." ReadOnly="True"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <label>E-mail Address</label>
                                        <div class="mb-3">
                                            <asp:TextBox class="form-control" ID="Email_Address" runat="server" placeholder="E-mail Address" ReadOnly="True"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>State</label>
                                        <div class="mb-3">
                                            <asp:TextBox class="form-control" ID="State" runat="server" placeholder="State" ReadOnly="True"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>City</label>
                                        <div class="mb-3">
                                            <asp:TextBox class="form-control" ID="City" runat="server" placeholder="City" ReadOnly="True"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <label>Full Postal Address</label>
                                        <div class="mb-3">
                                            <asp:TextBox class="form-control" ID="Full_Postal_Address" runat="server" placeholder="Full Postal Address" ReadOnly="true" TextMode="MultiLine" Rows="2"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-8 d-grid mx-auto">
                                        <asp:Button class="btn w-100 btn-danger btn-lg" ID="Delete_User_Permanently" runat="server" Text="Delete User Permanently" OnClick="Delete_User_Permanently_Click" />
                                    </div>  
                                </div>
                            </div>
                        </div>
                        <a href="homepage.aspx" class="btn-back"><i class="fas fa-long-arrow-alt-left"></i> Back to Homepage</a>
                    </div>            
                    <div class="col-md-7">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <div class="element-center">
                                            <h4>User List</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <hr />
                                    </div>
                                </div>
                                <div class="row">
                                    <asp:SqlDataSource ID="Users_Source" runat="server" ConnectionString="<%$ ConnectionStrings:BoovieStoreConnectionString %>" SelectCommand="SELECT * FROM [User]"></asp:SqlDataSource>
                                    <div class="col">
                                        <asp:GridView class="table table-striped table-bordered hover cell-border" ID="Users" runat="server" DataSourceID="Users_Source" AutoGenerateColumns="False">
                                            <Columns>
                                                <asp:BoundField DataField="id" HeaderText="User ID" ReadOnly="True" SortExpression="id" />
                                                <asp:BoundField DataField="name" HeaderText="Name" SortExpression="name" />
                                                <asp:BoundField DataField="account_status" HeaderText="Account Status" SortExpression="account_status" />
                                                <asp:BoundField DataField="email" HeaderText="E-mail Address" SortExpression="email" />
                                                <asp:BoundField DataField="contact_no" HeaderText="Contact No" SortExpression="contact_no" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
</asp:Content>
