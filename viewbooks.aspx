﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BoovieNested.master" AutoEventWireup="true" CodeBehind="viewbooks.aspx.cs" Inherits="BoovieStore.viewbooks" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    $(document).ready(function () {
        $(".table").prepend($("<thead></thead>").append($(this).find("tr:first"))).dataTable();
    });
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <main id="wrapper">
        <section class="has-margin-size-5">
            <div class="col-sm-12">
                <div class="element-center">
                    <h3>Our Books</h3>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <asp:Panel class="alert alert-success" role="alert" ID="Books_Panel" runat="server" Visible="false">
                            <asp:Label ID="Books_P_Label" runat="server" Text="Label"></asp:Label>
                        </asp:Panel>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <hr />
                                    </div>
                                </div>
                                <div class="row">
                                    <asp:SqlDataSource ID="Books_Source" runat="server" ConnectionString="<%$ ConnectionStrings:BoovieStoreConnectionString %>" SelectCommand="SELECT * FROM [Book]"></asp:SqlDataSource>
                                    <div class="col">
                                        <asp:GridView class="table table-striped table-bordered hover cell-border" ID="Books" runat="server" DataSourceID="Books_Source" AutoGenerateColumns="False" DataKeyNames="id">
                                            <Columns>
                                                <%-- <asp:BoundField DataField="id" HeaderText="ID" ReadOnly="true" SortExpression="id" /> --%>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <div class="container-fluid">
                                                            <div class="row">
                                                                <div class="col-lg-10">
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            <asp:Label ID="Book_title" runat="server" Text='<%# Eval("name") %>' Font-Bold="True" Font-Size="X-Large"></asp:Label><br /><br />
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            Author: <asp:Label ID="Book_author" runat="server" Text='<%# Eval("author_name") %>' Font-Bold="True" Font-Size="Medium"></asp:Label> |
                                                                            Genre(s): <asp:Label ID="Book_genre" runat="server" Text='<%# Eval("genre") %>' Font-Bold="True" Font-Size="Medium"></asp:Label> |
                                                                            Language: <asp:Label ID="Book_language" runat="server" Text='<%# Eval("language") %>' Font-Bold="True" Font-Size="Medium"></asp:Label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            Publisher: <asp:Label ID="Book_publisher" runat="server" Text='<%# Eval("publisher_name") %>' Font-Bold="True" Font-Size="Medium"></asp:Label> |
                                                                            Publish date: <asp:Label ID="Book_publish_date" runat="server" Text='<%# Eval("publish_date") %>' Font-Bold="True" Font-Size="Medium"></asp:Label> |
                                                                            Pages: <asp:Label ID="Book_pages" runat="server" Text='<%# Eval("pages") %>' Font-Bold="True" Font-Size="Medium"></asp:Label> |
                                                                            Edition: <asp:Label ID="Book_edition" runat="server" Text='<%# Eval("edition") %>' Font-Bold="True" Font-Size="Medium"></asp:Label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            Price: $<asp:Label ID="Book_cost" runat="server" Text='<%# Eval("price") %>' Font-Bold="True" Font-Size="Medium"></asp:Label> |
                                                                            In stock: <asp:Label ID="Book_actual" runat="server" Text='<%# Eval("actual_stock") %>' Font-Bold="True" Font-Size="Medium"></asp:Label> |
                                                                            Available: <asp:Label ID="Book_current" runat="server" Text='<%# Eval("current_stock") %>' Font-Bold="True" Font-Size="Medium"></asp:Label><br /><br />
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            Description: <asp:Label ID="Book_description" runat="server" Text='<%# Eval("description") %>' Font-Size="Small" Font-Italic="True" Font-Bold="True"></asp:Label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2">
                                                                    <asp:Image class="img-fluid" ID="Book_thumbnail" runat="server" ImageUrl='<%# Eval("image_link") %>' />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <a href="homepage.aspx" class="btn-back featurette-divider-bottom mt-4"><i class="fas fa-long-arrow-alt-left"></i> Back to Homepage</a>
        </section>
    </main>
</asp:Content>
