﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BoovieNested.Master" AutoEventWireup="true" CodeBehind="adminpublishermanagement.aspx.cs" Inherits="BoovieStore.adminpublishermanagement" %>

<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $(".table").prepend($("<thead></thead>").append($(this).find("tr:first"))).dataTable();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <main id="wrapper">
        <section class="has-margin-size-5">
            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <div class="element-center">
                                            <h4 class="d-block mx-auto">Publisher Details</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="element-center">
                                            <img width="100" src="imgs/nicolas.png"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <hr />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Publisher ID</label>
                                        <div class="mb-3">
                                            <div class="input-group">    
                                                <asp:TextBox class="form-control" ID="Publisher_ID" runat="server" placeholder="ID" TextMode="Number"></asp:TextBox>
                                                <asp:Button class="btn btn-primary" ID="Go" runat="server" Text="Go" OnClick="Go_Click"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <label>Publisher Name</label>
                                        <div class="mb-3">
                                            <asp:TextBox class="form-control" ID="Publisher_Name" runat="server" placeholder="Publisher Name"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-4 d-grid">
                                        <asp:Button class="btn w-100 btn-success btn-lg" ID="Add" runat="server" Text="Add" OnClick="Add_Click" />
                                    </div>  
                                    <div class="col-4 d-grid">
                                        <asp:Button class="btn w-100 btn-primary btn-lg" ID="Update" runat="server" Text="Update" OnClick="Update_Click" />
                                    </div>  
                                    <div class="col-4 d-grid">
                                        <asp:Button class="btn w-100 btn-danger btn-lg" ID="Delete" runat="server" Text="Delete" OnClick="Delete_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="homepage.aspx" class="btn-back"><i class="fas fa-long-arrow-alt-left"></i> Back to Homepage</a>
                    </div>            
                    <div class="col-md-7">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <div class="element-center">
                                            <h4>Publisher List</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <hr />
                                    </div>
                                </div>
                                <div class="row">
                                    <asp:SqlDataSource ID="Publishers_Source" runat="server" ConnectionString="<%$ ConnectionStrings:BoovieStoreConnectionString %>" SelectCommand="SELECT * FROM [Publisher]"></asp:SqlDataSource>
                                    <div class="col">
                                        <asp:GridView class="table table-striped table-bordered hover cell-border" ID="Publishers" runat="server" AutoGenerateColumns="False" DataKeyNames="id" DataSourceID="Publishers_Source">
                                            <Columns>
                                                <asp:BoundField DataField="id" HeaderText="Publisher ID" ReadOnly="True" SortExpression="id" />
                                                <asp:BoundField DataField="name" HeaderText="Publisher Name" SortExpression="name" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
</asp:Content>
