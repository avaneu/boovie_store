﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BoovieNested.Master" AutoEventWireup="true" CodeBehind="adminlogin.aspx.cs" Inherits="BoovieStore.adminlogin" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <main id="wrapper">
        <section class="has-margin-size-5">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 mx-auto">
                        <div class="card">
                            <div class="card-body">
                                <div class="element-center">
                                      <h3>Admin login</h3>
                                </div>
                           
                                  <div class="mb-3">
                                        <label class="label-text">Admin ID</label>
                                        <asp:TextBox class="form-control" ID="Admin_ID" runat="server" placeholder="Type in your ID"></asp:TextBox>
                                  </div>
                                   
                                 <div class="mb-3">
                                        <label class="label-text">Password</label>
                                        <asp:TextBox class="form-control" ID="Password" runat="server" placeholder="Type in your password" TextMode="Password"></asp:TextBox>
                                 </div>
                             
		                         <div class="mb-3 d-grid">
                                        <asp:Button class="btn btn-success btn-lg w-100 btn-top" ID="Login" runat="server" Text="Login" OnClick="Login_Click" />
                                 </div>            
                            </div>
                        </div>

                        <div class="mt-4">
                                    <a class="btn-back" href="homepage.aspx"><i class="fas fa-long-arrow-alt-left"></i> Back to Homepage </a>
                        </div>
                    </div>
                </div>
            </div>
      </section>
    </main>
</asp:Content>
