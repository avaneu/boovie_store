﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BoovieStore
{
    public partial class adminusermanagement : System.Web.UI.Page
    {
        string conn = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            string sessionName = Session["name"] as string;
            string sessionRole = Session["role"] as string;
            if (string.IsNullOrEmpty(sessionName)) {
                Response.Write("<script>alert('You are not logged in. Please login again to continue.');</script>");
                Response.Redirect("userlogin.aspx");
            }
            else if (sessionRole == "user") {
                Response.Redirect("userprofile.aspx");
            }
            Users.DataBind();
        }

        protected void Go_Click(object sender, EventArgs e)
        {
            GetUserByID();
        }

        protected void Activate_Click(object sender, EventArgs e)
        {
            UpdateUserAccountStatus("active");
        }

        protected void Suspend_Click(object sender, EventArgs e)
        {
            UpdateUserAccountStatus("pending");
        }

        protected void Deactivate_Click(object sender, EventArgs e)
        {
            UpdateUserAccountStatus("inactive");
        }

        protected void Delete_User_Permanently_Click(object sender, EventArgs e)
        {
            DeleteUser();
        }

        void GetUserByID()
        {
            try
            {
                SqlConnection con = new SqlConnection(conn);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }

                SqlCommand selectQuery = new SqlCommand("SELECT * FROM [dbo].[User] WHERE id = '" + User_ID.Text.Trim() + "';", con);

                SqlDataReader dr = selectQuery.ExecuteReader();

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {

                        Full_Name.Text = dr.GetValue(1).ToString();
                        Account_Status.Text = dr.GetValue(9).ToString();
                        Date_Of_Birth.Text = dr.GetValue(4).ToString();
                        Contact_No.Text = dr.GetValue(5).ToString();
                        Email_Address.Text = dr.GetValue(3).ToString();
                        State.Text = dr.GetValue(6).ToString();
                        City.Text = dr.GetValue(7).ToString();
                        Full_Postal_Address.Text = dr.GetValue(8).ToString();
                    }
                    /*Users.DataBind();*/
                }
                else
                {
                    Response.Write("<script>alert('User with such ID does not exist.')</script>");
                }

            }
            catch (Exception err) {
                Response.Write("<script>alert('" + err.Message + "')</script>");
            }
        }

        void UpdateUserAccountStatus(string status)
        {
            if (!DoesUserExist()) {
                Response.Write("<script>alert('User with such ID does not exist.')</script>");
            }
            else {
                try
                {
                    SqlConnection con = new SqlConnection(conn);
                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }

                    SqlCommand selectQuery = new SqlCommand("UPDATE [dbo].[User] SET account_status = '" + status + "' WHERE id = '" + User_ID.Text.Trim() + "';", con);

                    selectQuery.ExecuteNonQuery();
                    con.Close();
                    Users.DataBind();
                    Response.Write("<script>alert('User account status has been successfully updated!');</script>");
                }
                catch (Exception err)
                {
                    Response.Write("<script>alert('" + err.Message + "')</script>");
                }
            }
        }
        void DeleteUser()
        {
            if(!DoesUserExist()) {
                Response.Write("<script>alert('User with such ID does not exist.')</script>");
            }
            else {
                try
                {
                    SqlConnection con = new SqlConnection(conn);
                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }

                    SqlCommand deleteQuery = new SqlCommand("DELETE FROM [dbo].[User] WHERE id = '" + User_ID.Text.Trim() + "'", con);


                    deleteQuery.ExecuteNonQuery();
                    con.Close();
                    Response.Write("<script>alert('User has been successfully deleted!')</script>");
                    ClearForm();
                    Users.DataBind();
                }
                catch (Exception err)
                {
                    Response.Write("<script>alert('" + err.Message + "')</script>");
                }
            }
        }
        void ClearForm()
        {
            User_ID.Text = "";
            Full_Name.Text = "";
            Account_Status.Text = "";
            Date_Of_Birth.Text = "";
            Contact_No.Text = "";
            Email_Address.Text = "";
            State.Text = "";
            City.Text = "";
            Full_Postal_Address.Text = "";
        }

        bool DoesUserExist()
        {
            try
            {
                SqlConnection con = new SqlConnection(conn);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }

                SqlCommand selectQuery = new SqlCommand("SELECT * FROM [dbo].[User] WHERE id = '" + User_ID.Text.Trim() + "';", con);

                SqlDataAdapter da = new SqlDataAdapter(selectQuery);

                DataTable dt = new DataTable();

                da.Fill(dt);

                if (dt.Rows.Count >= 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception err)
            {
                Response.Write("<script>alert('" + err.Message + "')</script>");
                return false;
            }
        }
    }
}