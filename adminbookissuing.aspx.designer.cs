﻿//------------------------------------------------------------------------------
// <generowany automatycznie>
//     Ten kod został wygenerowany przez narzędzie.
//
//     Modyfikacje tego pliku mogą spowodować niewłaściwe zachowanie i zostaną utracone
//     w przypadku ponownego wygenerowania kodu. 
// </generowany automatycznie>
//------------------------------------------------------------------------------

namespace BoovieStore
{


    public partial class adminbookissuing
    {

        /// <summary>
        /// Kontrolka User_ID.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox User_ID;

        /// <summary>
        /// Kontrolka Book_ID.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox Book_ID;

        /// <summary>
        /// Kontrolka Go.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button Go;

        /// <summary>
        /// Kontrolka User_Name.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox User_Name;

        /// <summary>
        /// Kontrolka Book_Title.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox Book_Title;

        /// <summary>
        /// Kontrolka Issue_Date.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox Issue_Date;

        /// <summary>
        /// Kontrolka Due_Date.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox Due_Date;

        /// <summary>
        /// Kontrolka Issue.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button Issue;

        /// <summary>
        /// Kontrolka Return.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button Return;

        /// <summary>
        /// Kontrolka Issued_Source.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.SqlDataSource Issued_Source;

        /// <summary>
        /// Kontrolka Issued.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.GridView Issued;
    }
}
