﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BoovieStore
{
    public partial class Boovie : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try {
                //if (Session["role"].Equals("")) {
                string sessionRole = Session["role"] as string;
                if (string.IsNullOrEmpty(sessionRole)) { 
                    User_Login.Visible = true;
                    Sign_Up.Visible = true;
                    Logout.Visible = false;
                    Hello_User.Visible = false;
                }
                else if(Session["role"].Equals("user")) {
                    User_Login.Visible = false;
                    Sign_Up.Visible = false;
                    Logout.Visible = true;
                    Hello_User.Visible = true;
                    Hello_User.Text = "Hi, " + Session["id"];
                }
                else if (Session["role"].Equals("admin"))
                {
                    User_Login.Visible = false;
                    Sign_Up.Visible = false;
                    Logout.Visible = true;
                    Hello_User.Visible = true;
                    Hello_User.Text = "Hi, " + Session["id"];
                }
            }
            catch (Exception err) {

            }
        }
        protected void View_Books_Click(object sender, EventArgs e)
        {
            Response.Redirect("viewbooks.aspx");
        }
        protected void User_Login_Click(object sender, EventArgs e)
        {
            Response.Redirect("userlogin.aspx");
        }
        protected void Sign_Up_Click(object sender, EventArgs e)
        {
            Response.Redirect("signup.aspx");
        }
        protected void Logout_Click(object sender, EventArgs e)
        {
            Session["id"] = "";
            Session["name"] = "";
            Session["status"] = "";
            Session["role"] = "";

            Response.Redirect("homepage.aspx");
            
        }
        protected void Hello_User_Click(object sender, EventArgs e)
        {
            Response.Redirect("userprofile.aspx");
        }
    }
}