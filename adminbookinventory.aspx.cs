﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BoovieStore
{
    public partial class adminbookinventory : System.Web.UI.Page
    {
        string conn = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
        static string g_filepath;
        static int g_actual, g_current, g_issued;
        protected void Page_Load(object sender, EventArgs e)
        {
            string sessionName = Session["name"] as string;
            string sessionRole = Session["role"] as string;
            if (string.IsNullOrEmpty(sessionName))
            {
                Response.Write("<script>alert('You are not logged in. Please login again to continue.');</script>");
                Response.Redirect("userlogin.aspx");
            }
            else if (sessionRole == "user") {
                Response.Redirect("userprofile.aspx");
            }
            if (!IsPostBack) {
                FillAPDetails();
            }
            Books.DataBind();
        }
        protected void Go_Click(object sender, EventArgs e)
        {
            GetBookByID();
        }
        protected void Add_Click(object sender, EventArgs e)
        {
            if (DoesBookExist()) {
                Response.Write("<script>alert('Book with such ID or title already exists!');</script>");
            }
            else {
                NewBook();
            }
        }

        protected void Update_Click(object sender, EventArgs e)
        {
            UpdateBook();
        }

        protected void Delete_Click(object sender, EventArgs e)
        {
            DeleteBook();
        }

        void FillAPDetails()
        {
            try
            {
                SqlConnection con = new SqlConnection(conn);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }

                SqlCommand selectQuery = new SqlCommand("SELECT name FROM [dbo].[Author];", con);

                SqlDataAdapter da = new SqlDataAdapter(selectQuery);
                DataTable dt = new DataTable();
                da.Fill(dt);

                Authors.DataSource = dt;
                Authors.DataValueField = "name";
                Authors.DataBind();

                selectQuery = new SqlCommand("SELECT name FROM [dbo].[Publisher];", con);
                da = new SqlDataAdapter(selectQuery);
                dt = new DataTable();
                da.Fill(dt);

                Publishers.DataSource = dt;
                Publishers.DataValueField = "name";
                Publishers.DataBind();
            }
            catch (Exception err)
            {
                Response.Write("<script>alert('" + err.Message + "');</script>");
            }
        }

        bool DoesBookExist()
        {
            try
            {
                SqlConnection con = new SqlConnection(conn);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }

                SqlCommand selectQuery = new SqlCommand("SELECT * FROM [dbo].[Book] WHERE id = '" + Book_ID.Text.Trim() + "' OR name = '" + Book_Name.Text.Trim() + "';", con);

                SqlDataAdapter da = new SqlDataAdapter(selectQuery);

                DataTable dt = new DataTable();

                da.Fill(dt);

                if (dt.Rows.Count >= 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception err)
            {
                Response.Write("<script>alert('" + err.Message + "')</script>");
                return false;
            }
        }

        void NewBook() {
            try {
                string genres = "";
                foreach (int i in Genres.GetSelectedIndices()) {
                    genres += Genres.Items[i] + ",";
                }
                genres = genres.Remove(genres.Length - 1);

                string imagePath = "~/imgs/books/book_default.jpg";
                string imageName = Path.GetFileName(Book_Photo.PostedFile.FileName);
                Book_Photo.SaveAs(Server.MapPath("imgs/books/" + imageName));
                imagePath = "~/imgs/books/" + imageName;


                SqlConnection con = new SqlConnection(conn);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }

                SqlCommand insertQuery = new SqlCommand("INSERT INTO [dbo].[Book](id, name, genre, author_name, publisher_name, publish_date, language, edition, price, pages, description, actual_stock, current_stock, image_link) VALUES(@id, @name, @genre, @author_name, @publisher_name, @publish_date, @language, @edition, @price, @pages, @description, @actual_stock, @current_stock, @image_link)", con);

                insertQuery.Parameters.AddWithValue("@id", Book_ID.Text.Trim());
                insertQuery.Parameters.AddWithValue("@name", Book_Name.Text.Trim());
                insertQuery.Parameters.AddWithValue("@genre", genres);
                insertQuery.Parameters.AddWithValue("@author_name", Authors.SelectedItem.Value);
                insertQuery.Parameters.AddWithValue("@publisher_name", Publishers.SelectedItem.Value);
                insertQuery.Parameters.AddWithValue("@publish_date", Date.Text.Trim());
                insertQuery.Parameters.AddWithValue("@language", Languages.SelectedItem.Value);
                insertQuery.Parameters.AddWithValue("@edition", Edition.Text.Trim());
                insertQuery.Parameters.AddWithValue("@price", Price.Text.Trim());
                insertQuery.Parameters.AddWithValue("@pages", Pages.Text.Trim());
                insertQuery.Parameters.AddWithValue("@description", Book_Description.Text.Trim());
                insertQuery.Parameters.AddWithValue("@actual_stock", Actual_Stock.Text.Trim());
                insertQuery.Parameters.AddWithValue("@current_stock", Actual_Stock.Text.Trim());
                insertQuery.Parameters.AddWithValue("@image_link", imagePath);

                insertQuery.ExecuteNonQuery();
                con.Close();
                Response.Write("<script>alert('Book has been successfully added!')</script>");
                Books.DataBind();
            }
            catch (Exception err) {
                Response.Write("<script>alert('" + err.Message + "')</script>");
            }
        }

        void GetBookByID()
        {
            try
            {
                SqlConnection con = new SqlConnection(conn);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }

                SqlCommand selectQuery = new SqlCommand("SELECT * FROM [dbo].[Book] WHERE id = '" + Book_ID.Text.Trim() + "';", con);
                SqlDataAdapter da = new SqlDataAdapter(selectQuery);
                DataTable dt = new DataTable();

                da.Fill(dt);

                if(dt.Rows.Count >= 1) {
                    Book_Name.Text = dt.Rows[0][1].ToString();
                    Languages.SelectedValue = dt.Rows[0][6].ToString().Trim();
                    Authors.SelectedValue = dt.Rows[0][3].ToString().Trim();
                    Publishers.SelectedValue = dt.Rows[0][4].ToString().Trim();
                    Date.Text = dt.Rows[0][5].ToString();
                    Edition.Text = dt.Rows[0][7].ToString();
                    Price.Text = dt.Rows[0][8].ToString().Trim();
                    Pages.Text = dt.Rows[0][9].ToString().Trim();
                    Actual_Stock.Text = dt.Rows[0][11].ToString().Trim();
                    Current_Stock.Text = dt.Rows[0][12].ToString().Trim();
                    Issued_Books.Text = "" + (Convert.ToInt32(dt.Rows[0][11].ToString()) - Convert.ToInt32(dt.Rows[0][12].ToString()));
                    Book_Description.Text = dt.Rows[0][10].ToString();

                    Genres.ClearSelection();
                    
                    string[] genre = dt.Rows[0][2].ToString().Trim().Split(',');
                    for(int i=0; i<genre.Length; i++) {
                        for(int j=0; j<Genres.Items.Count; j++) {
                            if(Genres.Items[j].ToString() == genre[i]) {
                                Genres.Items[j].Selected = true;
                            }
                        }
                    }

                    g_actual = Convert.ToInt32(dt.Rows[0][11].ToString().Trim());
                    g_current = Convert.ToInt32(dt.Rows[0][12].ToString().Trim());
                    g_issued = g_actual - g_current;
                    g_filepath = dt.Rows[0][13].ToString();
                }
                else {
                    Response.Write("<script>alert('Book with such ID does not exist!')</script>");
                }

            }
            catch (Exception err)
            {
                Response.Write("<script>alert('" + err.Message + "')</script>");
            }
        }

        void UpdateBook()
        {
            if (!DoesBookExist())
            {
                Response.Write("<script>alert('Book with such ID does not exist.')</script>");
            }
            else
            {
                try
                {
                    int actual = Convert.ToInt32(Actual_Stock.Text.Trim());
                    int current = Convert.ToInt32(Current_Stock.Text.Trim());
                    if(g_actual == actual) {

                    }
                    else {
                        if(actual < g_issued) {
                            Response.Write("<script>alert('Actual stock value cannot be lower than Issued stock value. (sic!)')</script>");
                            return;
                        }
                        else {
                            current = actual - g_issued;
                            Issued_Books.Text = "" + current;
                        }
                    }


                    string genres = "";
                    foreach (int i in Genres.GetSelectedIndices())
                    {
                        genres += Genres.Items[i] + ",";
                    }
                    genres = genres.Remove(genres.Length - 1);


                    string imagePath = "~/imgs/books/book_default.jpg";
                    string imageName = Path.GetFileName(Book_Photo.PostedFile.FileName);
                    if(imageName == "" || imageName == null) {
                        imagePath = g_filepath;
                    } 
                    else {
                        Book_Photo.SaveAs(Server.MapPath("imgs/books/" + imageName));
                        imagePath = "~/imgs/books/" + imageName;
                    }


                    SqlConnection con = new SqlConnection(conn);
                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }

                    SqlCommand updateQuery = new SqlCommand("UPDATE [dbo].[Book] SET name = @name, genre = @genre, author_name = @author_name, publisher_name = @publisher_name, publish_date = @publish_date, language = @language, edition = @edition, price = @price, pages = @pages, description = @description, actual_stock = @actual_stock, current_stock = @current_stock, image_link = @image_link WHERE id = '" + Book_ID.Text.Trim() + "'",con);

                    updateQuery.Parameters.AddWithValue("@name", Book_Name.Text.Trim());
                    updateQuery.Parameters.AddWithValue("@genre", genres);
                    updateQuery.Parameters.AddWithValue("@author_name", Authors.SelectedItem.Value);
                    updateQuery.Parameters.AddWithValue("@publisher_name", Publishers.SelectedItem.Value);
                    updateQuery.Parameters.AddWithValue("@publish_date", Date.Text.Trim());
                    updateQuery.Parameters.AddWithValue("@language", Languages.SelectedItem.Value);
                    updateQuery.Parameters.AddWithValue("@edition", Edition.Text.Trim());
                    updateQuery.Parameters.AddWithValue("@price", Price.Text.Trim());
                    updateQuery.Parameters.AddWithValue("@pages", Pages.Text.Trim());
                    updateQuery.Parameters.AddWithValue("@description", Book_Description.Text.Trim());
                    updateQuery.Parameters.AddWithValue("@actual_stock", actual.ToString());
                    updateQuery.Parameters.AddWithValue("@current_stock", current.ToString());
                    updateQuery.Parameters.AddWithValue("@image_link", imagePath);

                    updateQuery.ExecuteNonQuery();
                    con.Close();
                    Books.DataBind();
                    Response.Write("<script>alert('Book details has been successfully updated!');</script>");
                }
                catch (Exception err)
                {
                    Response.Write("<script>alert('" + err.Message + "')</script>");
                }
            }
        }
        void DeleteBook()
        {
            if (!DoesBookExist())
            {
                Response.Write("<script>alert('Book with such ID does not exist.')</script>");
            }
            else
            {
                try
                {
                    SqlConnection con = new SqlConnection(conn);
                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }

                    SqlCommand deleteQuery = new SqlCommand("DELETE FROM [dbo].[Book] WHERE id = '" + Book_ID.Text.Trim() + "'", con);


                    deleteQuery.ExecuteNonQuery();
                    con.Close();
                    Response.Write("<script>alert('Book has been successfully deleted!')</script>");
                    Books.DataBind();
                }
                catch (Exception err)
                {
                    Response.Write("<script>alert('" + err.Message + "')</script>");
                }
            }
        }
    }
}