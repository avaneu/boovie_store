﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BoovieStore
{
    public partial class adminlogin : System.Web.UI.Page
    {
        string conn = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Login_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection con = new SqlConnection(conn);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }

                SqlCommand selectQuery = new SqlCommand("SELECT * FROM [dbo].[Admin_login] WHERE username = '" + Admin_ID.Text.Trim() + "' AND password = '" + Password.Text.Trim() + "';", con);

                SqlDataReader dr = selectQuery.ExecuteReader();

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Response.Write("<script>alert('Login successful. Redirecting to homepage')</script>");
                        //Response.Write("<script>alert('" + dr.GetValue(0).ToString() + "')</script>");
                        Session["id"] = dr.GetValue(0).ToString();
                        Session["name"] = dr.GetValue(2).ToString();
                        Session["role"] = "admin";
                    }
                    Response.Redirect("homepage.aspx");
                }
                else
                {
                    Response.Write("<script>alert('Invalid login credentials.')</script>");
                }
            }
            catch (Exception err)
            {
                Response.Write("<script>alert('" + err.Message + "')</script>");
            }
        }
    }
}