﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BoovieNested.Master" AutoEventWireup="true" CodeBehind="adminbookinventory.aspx.cs" Inherits="BoovieStore.adminbookinventory" %>

<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function getURL(file) {
            if (file.files && file.files[0]) {
                var fr = new FileReader();
                fr.onload = function (event) {
                    $('#book_img').attr('src', event.target.result);
                };
                fr.readAsDataURL(file.files[0]);
            }
        }

        $(document).ready(function () {
            $(".table").prepend($("<thead></thead>").append($(this).find("tr:first"))).dataTable();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <main id="wrapper">
        <section class="has-margin-size-5">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-5">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <div class="element-center">
                                            <h4 class="d-block mx-auto">Book Details</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="element-center">
                                            <img id="book_img" width="100" src="imgs/books.png" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <hr />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:FileUpload ID="Book_Photo" class="form-control" runat="server" onChange="getURL(this);"></asp:FileUpload>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>Book ID</label>
                                        <div class="mb-3">
                                            <div class="input-group">
                                                <asp:TextBox class="form-control" ID="Book_ID" runat="server" placeholder="ID" TextMode="Number"></asp:TextBox>
                                                <asp:LinkButton class="btn btn-primary" ID="Go" runat="server" Text="" OnClick="Go_Click">
                                                    <i class="fas fa-check-circle"></i>
                                                </asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <label>Book Name</label>
                                        <div class="mb-3">
                                            <asp:TextBox class="form-control" ID="Book_Name" runat="server" placeholder="Book Name"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Language</label>
                                        <div class="mb-3">
                                            <asp:DropDownList ID="Languages" class="form-control" runat="server">
                                                <asp:ListItem Text="English" Value="English" />
                                                <asp:ListItem Text="German" Value="German" />
                                                <asp:ListItem Text="French" Value="French" />
                                                <asp:ListItem Text="Polish" Value="Polish" />
                                                <asp:ListItem Text="Spanish" Value="Spanish" />
                                                <asp:ListItem Text="Italian" Value="Italian" />
                                            </asp:DropDownList>
                                        </div>
                                        <label>Publisher Name</label>
                                        <div class="mb-3">
                                            <asp:DropDownList ID="Publishers" class="form-control" runat="server">
                                                <%-- <asp:ListItem Text="Publisher 1" Value="Publisher 1" />
                                                <asp:ListItem Text="Publisher 2" Value="Publisher 2" /> --%>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Author Name</label>
                                        <div class="mb-3">
                                            <asp:DropDownList ID="Authors" class="form-control" runat="server">
                                                <%-- <asp:ListItem Text="A1" Value="A1" />
                                                <asp:ListItem Text="A2" Value="A2" /> --%>
                                            </asp:DropDownList>
                                        </div>
                                        <label>Publish Date</label>
                                        <div class="mb-3">
                                            <asp:TextBox class="form-control" ID="Date" runat="server" placeholder="Date" TextMode="Date"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Genre</label>
                                        <div class="mb-3">
                                            <asp:ListBox class="form-control" ID="Genres" runat="server" SelectionMode="Multiple" Rows="5">
                                                <asp:ListItem Text="Adventure" Value="Adventure" />
                                                <asp:ListItem Text="Comic Book" Value="Comic Book" />
                                                <asp:ListItem Text="Horror" Value="Horror" />
                                                <asp:ListItem Text="Crime" Value="Crime" />
                                                <asp:ListItem Text="Fantasy" Value="Fantasy" />
                                                <asp:ListItem Text="Autobiography" Value="Autobiography" />
                                                <asp:ListItem Text="Motivation" Value="Motivation" />
                                                <asp:ListItem Text="Romance" Value="Romance" />
                                                <asp:ListItem Text="Science" Value="Science" />
                                                <asp:ListItem Text="Poetry" Value="Poetry" />
                                                <asp:ListItem Text="Guides" Value="Guides" />
                                                <asp:ListItem Text="Programming" Value="Programming" />
                                            </asp:ListBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Edition</label>
                                        <div class="mb-3">
                                            <asp:TextBox class="form-control" ID="Edition" runat="server" placeholder="Edition"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Price</label>
                                        <div class="mb-3">
                                            <asp:TextBox class="form-control" ID="Price" runat="server" placeholder="Price" TextMode="Number"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Pages</label>
                                        <div class="mb-3">
                                            <asp:TextBox class="form-control" ID="Pages" runat="server" placeholder="Pages" TextMode="Number"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Actual Stock</label>
                                        <div class="mb-3">
                                            <asp:TextBox class="form-control" ID="Actual_Stock" runat="server" placeholder="Actual Stock" TextMode="Number"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Current Stock</label>
                                        <div class="mb-3">
                                            <asp:TextBox class="form-control" ID="Current_Stock" runat="server" placeholder="Current Stock" TextMode="Number" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Issued Books</label>
                                        <div class="mb-3">
                                            <asp:TextBox class="form-control" ID="Issued_Books" runat="server" placeholder="Issued Books" TextMode="Number" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <label>Book Description</label>
                                        <div class="mb-3">
                                            <asp:TextBox class="form-control" ID="Book_Description" runat="server" placeholder="Book Description" TextMode="MultiLine" Rows="2"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <asp:Button class="btn w-100 btn-success btn-lg" ID="Add" runat="server" Text="Add" OnClick="Add_Click" />
                                    </div>
                                    <div class="col-md-4">
                                        <asp:Button class="btn w-100 btn-primary btn-lg" ID="Update" runat="server" Text="Update" OnClick="Update_Click" />
                                    </div>
                                    <div class="col-md-4">
                                        <asp:Button class="btn w-100 btn-danger btn-lg" ID="Delete" runat="server" Text="Delete" OnClick="Delete_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="homepage.aspx" class="btn-back"><i class="fas fa-long-arrow-alt-left"></i> Back to Homepage</a>
                    </div>
                    <div class="col-md-7">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <div class="element-center">
                                            <h4>Book Inventory List</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <hr />
                                    </div>
                                </div>
                                <div class="row">
                                    <asp:SqlDataSource ID="Books_Source" runat="server" ConnectionString="<%$ ConnectionStrings:BoovieStoreConnectionString %>" SelectCommand="SELECT * FROM [Book]"></asp:SqlDataSource>
                                    <div class="col">
                                        <asp:GridView class="table table-striped table-bordered hover cell-border" ID="Books" runat="server" DataSourceID="Books_Source" AutoGenerateColumns="False" DataKeyNames="id">
                                            <Columns>
                                                <asp:BoundField DataField="id" HeaderText="ID" ReadOnly="true" SortExpression="id" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <div class="container-fluid">
                                                            <div class="row">
                                                                <div class="col-lg-10">
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            <asp:Label ID="Book_title" runat="server" Text='<%# Eval("name") %>' Font-Bold="True" Font-Size="X-Large"></asp:Label><br /><br />
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            Author: <asp:Label ID="Book_author" runat="server" Text='<%# Eval("author_name") %>' Font-Bold="True" Font-Size="Medium"></asp:Label> |
                                                                            Genre(s): <asp:Label ID="Book_genre" runat="server" Text='<%# Eval("genre") %>' Font-Bold="True" Font-Size="Medium"></asp:Label> |
                                                                            Language: <asp:Label ID="Book_language" runat="server" Text='<%# Eval("language") %>' Font-Bold="True" Font-Size="Medium"></asp:Label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            Publisher: <asp:Label ID="Book_publisher" runat="server" Text='<%# Eval("publisher_name") %>' Font-Bold="True" Font-Size="Medium"></asp:Label> |
                                                                            Publish date: <asp:Label ID="Book_publish_date" runat="server" Text='<%# Eval("publish_date") %>' Font-Bold="True" Font-Size="Medium"></asp:Label> |
                                                                            Pages: <asp:Label ID="Book_pages" runat="server" Text='<%# Eval("pages") %>' Font-Bold="True" Font-Size="Medium"></asp:Label> |
                                                                            Edition: <asp:Label ID="Book_edition" runat="server" Text='<%# Eval("edition") %>' Font-Bold="True" Font-Size="Medium"></asp:Label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            Price: $<asp:Label ID="Book_cost" runat="server" Text='<%# Eval("price") %>' Font-Bold="True" Font-Size="Medium"></asp:Label> |
                                                                            In stock: <asp:Label ID="Book_actual" runat="server" Text='<%# Eval("actual_stock") %>' Font-Bold="True" Font-Size="Medium"></asp:Label> |
                                                                            Available: <asp:Label ID="Book_current" runat="server" Text='<%# Eval("current_stock") %>' Font-Bold="True" Font-Size="Medium"></asp:Label><br /><br />
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            Description: <asp:Label ID="Book_description" runat="server" Text='<%# Eval("description") %>' Font-Size="Small" Font-Italic="True" Font-Bold="True"></asp:Label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2">
                                                                    <asp:Image class="img-fluid" ID="Book_thumbnail" runat="server" ImageUrl='<%# Eval("image_link") %>' />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
</asp:Content>
