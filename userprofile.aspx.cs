﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BoovieStore
{
    public partial class userprofile : System.Web.UI.Page
    {
        string conn = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            
            try {
                string sessionName = Session["name"] as string;
                string sessionRole = Session["role"] as string;
                if(sessionRole == "admin") {
                    Response.Redirect("adminusermanagement.aspx");
                }
                else if (string.IsNullOrEmpty(sessionName)) {
                    Response.Write("<script>alert('You are not logged in. Please login again to continue.');</script>");
                    Response.Redirect("userlogin.aspx");
                }
                else {
                    GetUserIssuedDetails();
                    if (!Page.IsPostBack) {
                        GetUserDetails();
                    }
                }
            }
            catch(Exception err) {
                Response.Write("<script>alert('" + err.Message + "');</script>");
            }
        }

        protected void Update_Click(object sender, EventArgs e)
        {
            string sessionName = Session["name"] as string;
            if (string.IsNullOrEmpty(sessionName))
            {
                Response.Write("<script>alert('Something has failed. Please login again to continue.');</script>");
                Response.Redirect("userlogin.aspx");
            }
            else
            {
                UpdateUser();
            }
            
        }

        void GetUserIssuedDetails()
        {
            try {
                SqlConnection con = new SqlConnection(conn);
                if (con.State == ConnectionState.Closed) {
                    con.Open();
                }

                SqlCommand selectQuery = new SqlCommand("SELECT * FROM [dbo].[BookIssuing] WHERE user_id = '" + Session["id"].ToString() + "'", con);
                SqlDataAdapter da = new SqlDataAdapter(selectQuery);
                DataTable dt = new DataTable();
                da.Fill(dt);

                Issued.DataSource = dt;
                Issued.DataBind();
                
            }
            catch (Exception err) {
                Response.Write("<script>alert('" + err.Message + "');</script>");
            }
        }

        protected void Issued_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try {
                if (e.Row.RowType == DataControlRowType.DataRow) {
                    DateTime due = Convert.ToDateTime(e.Row.Cells[5].Text);
                    DateTime today = DateTime.Today;
                    if (today > due) {
                        e.Row.BackColor = System.Drawing.Color.DeepPink;
                    }
                }
            }
            catch (Exception err) {
                Response.Write("<script>alert('" + err.Message + "')</script>");
            }
        }

        void GetUserDetails()
        {
            try {
                SqlConnection con = new SqlConnection(conn);
                if (con.State == ConnectionState.Closed) {
                    con.Open();
                }

                SqlCommand selectQuery = new SqlCommand("SELECT * FROM [dbo].[User] WHERE id = '" + Session["id"].ToString() + "'", con);
                SqlDataAdapter da = new SqlDataAdapter(selectQuery);
                DataTable dt = new DataTable();
                da.Fill(dt);

                Full_Name.Text = dt.Rows[0][1].ToString();
                Date_Of_Birth.Text = dt.Rows[0][4].ToString();
                Contact_No.Text = dt.Rows[0][5].ToString();
                Email_Address.Text = dt.Rows[0][3].ToString();
                States.SelectedValue = dt.Rows[0][6].ToString().Trim();
                City.Text = dt.Rows[0][7].ToString();
                Full_Address.Text = dt.Rows[0][8].ToString();
                User_ID.Text = dt.Rows[0][0].ToString();
                Old_Password.Text = dt.Rows[0][2].ToString();

                Status.Text = dt.Rows[0][9].ToString().Trim();
                if (dt.Rows[0][9].ToString().Trim() == "active")
                    Status.Attributes.Add("class", "badge badge-pill badge-success");
                else if (dt.Rows[0][9].ToString().Trim() == "pending")
                    Status.Attributes.Add("class", "badge badge-pill badge-warning");
                else if (dt.Rows[0][9].ToString().Trim() == "inactive")
                    Status.Attributes.Add("class", "badge badge-pill badge-danger");
                else
                    Status.Attributes.Add("class", "badge badge-pill badge-secondary");
            }
            catch (Exception err)
            {
                Response.Write("<script>alert('" + err.Message + "');</script>");
            }
        }

        void UpdateUser() {
            try {
                string pass = "";
                string old_pass = New_Password.Text.Trim();
                if (string.IsNullOrEmpty(old_pass)) {
                    pass = old_pass;
                }
                else {
                    pass = New_Password.Text.Trim();
                }

                SqlConnection con = new SqlConnection(conn);
                if (con.State == ConnectionState.Closed) {
                    con.Open();
                }

                SqlCommand updateQuery = new SqlCommand("UPDATE [dbo].[User] SET name = @name, password = @password, email = @email, date_of_birth = @date_of_birth, contact_no = @contact_no, state = @state, city = @city, full_address = @full_address, account_status = @account_status WHERE id = '" + Session["id"].ToString().Trim() + "'", con);

                updateQuery.Parameters.AddWithValue("@name", Full_Name.Text.Trim());
                updateQuery.Parameters.AddWithValue("@password", pass);
                updateQuery.Parameters.AddWithValue("@email", Email_Address.Text.Trim());
                updateQuery.Parameters.AddWithValue("@date_of_birth", Date_Of_Birth.Text.Trim());
                updateQuery.Parameters.AddWithValue("@contact_no", Contact_No.Text.Trim());
                updateQuery.Parameters.AddWithValue("@state", States.SelectedItem.Value);
                updateQuery.Parameters.AddWithValue("@city", City.Text.Trim());
                updateQuery.Parameters.AddWithValue("@full_address", Full_Address.Text.Trim());
                updateQuery.Parameters.AddWithValue("@account_status", "pending");

                int res = updateQuery.ExecuteNonQuery();
                con.Close();
                if(res > 0) {
                    Response.Write("<script>alert('Account details have been successfully updated!');</script>");
                    GetUserDetails();
                    GetUserIssuedDetails();
                }
                else {
                    Response.Write("<script>alert('User with such ID does not exist!');</script>");
                }
            }
            catch(Exception err) {
                Response.Write("<script>alert('" + err.Message + "');</script>");
            }
        }
    }
}