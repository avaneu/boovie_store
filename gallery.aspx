﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BoovieNested.master" AutoEventWireup="true" CodeBehind="gallery.aspx.cs" Inherits="BoovieStore.gallery" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <main id="wrapper">
        <section class="has-margin-size-5">
            <div class="container">
                <div class="row">
                    <div class="col-12 mx-auto">
                        <h2 class="element-center">Book Inventory List</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <asp:GridView class="table table-striped table-bordered" ID="GridView1" runat="server"></asp:GridView>
                        <a class="btn-back featurette-divider-bottom" href="homepage.aspx"><i class="fas fa-long-arrow-alt-left"></i> Back to Homepage</a>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="container">
              <div class="row">
                <a href="imgs/books/gallery1.jpg" data-gallery="gallery" class="col-md-4 mb-4">
                  <img src="imgs/books/gallery1.jpg" class="img-fluid rounded">
                </a>
                <a href="imgs/books/gallery4.jpg" data-gallery="gallery" class="col-md-4 mb-4">
                  <img src="imgs/books/gallery4.jpg" class="img-fluid rounded">
                </a>
                <a href="imgs/books/gallery3.jpg" data-gallery="gallery" class="col-md-4 mb-4">
                  <img src="imgs/books/gallery3.jpg" class="img-fluid rounded">
                </a>
              </div>
              <div class="row">
                <a href="imgs/books/gallery5.jpg" data-gallery="gallery" class="col-md-4">
                  <img src="imgs/books/gallery5.jpg" class="img-fluid rounded">
                </a>
                <a href="imgs/books/gallery2.jpg" data-gallery="gallery" class="col-md-4">
                  <img src="imgs/books/gallery2.jpg" class="img-fluid rounded">
                </a>
                <a href="imgs/books/gallery6.jpg" data-gallery="gallery" class="col-md-4">
                  <img src="imgs/books/gallery6.jpg" class="img-fluid rounded">
                </a>
              </div>
            </div>
        </section>
    </main>
</asp:Content>
