﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BoovieStore
{
    public partial class BoovieNested : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try {
                string sessionRole = Session["role"] as string;
                if (string.IsNullOrEmpty(sessionRole)) {
                    Admin_login.Visible = true;
                    Author_Management.Visible = false;
                    Publisher_Management.Visible = false;
                    Book_Inventory.Visible = false;
                    Book_Issuing.Visible = false;
                    User_Management.Visible = false;
                }
                else if (Session["role"].Equals("user")) {
                    Admin_login.Visible = false;
                    Author_Management.Visible = false;
                    Publisher_Management.Visible = false;
                    Book_Inventory.Visible = false;
                    Book_Issuing.Visible = false;
                    User_Management.Visible = false;
                }
                else if (Session["role"].Equals("admin")) {
                    Admin_login.Visible = false;
                    Author_Management.Visible = true;
                    Publisher_Management.Visible = true;
                    Book_Inventory.Visible = true;
                    Book_Issuing.Visible = true;
                    User_Management.Visible = true;
                }
            }
            catch (Exception err) {

            }
        }

        protected void Admin_Login_Click(object sender, EventArgs e)
        {
            Response.Redirect("adminlogin.aspx");
        }
        protected void Author_Management_Click(object sender, EventArgs e)
        {
            Response.Redirect("adminauthormanagement.aspx");
        }
        protected void Publisher_Management_Click(object sender, EventArgs e)
        {
            Response.Redirect("adminpublishermanagement.aspx");
        }
        protected void Book_Inventory_Click(object sender, EventArgs e)
        {
            Response.Redirect("adminbookinventory.aspx");
        }
        protected void Book_Issuing_Click(object sender, EventArgs e)
        {
            Response.Redirect("adminbookissuing.aspx");
        }
        protected void User_Management_Click(object sender, EventArgs e)
        {
            Response.Redirect("adminusermanagement.aspx");
        }
    }
}