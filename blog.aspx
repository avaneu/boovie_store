﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BoovieNested.master" AutoEventWireup="true" CodeBehind="blog.aspx.cs" Inherits="BoovieStore.blog" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <main id="wrapper">
    <section class="bgimage-about">
        <div class="container-fluid">
            <div class="row">
                <div>
                    <div class="hero-text">
                    <h2 class="mb-4">Blog</h2>
                    <p>Here you will find a fascinating world of books - its gates are open to you, just reach for it.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>  
    
    <section class="featurette-blog">
        <div class="container">
        <div class="row mb-0 featurette-divider">
        <div class="col-md-12">
          <div class="card card-border flex-md-row mb-4 box-shadow h-md-250">
            <div class="card-body d-flex flex-column align-items-start">
              <strong class="d-inline-block mb-0 text-primary">10 February 2020</strong>
              <h3 class="mb-4">
                <a class="text-dark" href="#">Harry Potter and Philosopher's Stone</a>
              </h3>
              <p class="card-text mb-auto">It was first published in Britain in 1997 and appeared in the United States the following year under the title Harry Potter and the Sorcerer’s Stone...</p>
              <div class="btn btn-sm btn-outline-secondary btn-white-style"><a href="blog_page.aspx">Continue reading</a></div>
            </div>
            <img class="card-img-right flex-auto d-none d-md-block opacity-blog" src="imgs/blog1.jpg"/>
          </div>
        </div>
            <div class="col-md-12">
          <div class="card card-border flex-md-row mb-4 box-shadow h-md-250">
            <div class="card-body d-flex flex-column align-items-start">
              <strong class="d-inline-block mb-0 text-primary">11 February 2020</strong>
              <h3 class="mb-4">
                <a class="text-dark" href="#">Lion, the Witch and the Wardrobe</a>
              </h3>
              <p class="card-text mb-auto">The Lion, the Witch and the Wardrobe is a fantasy novel for children by C. S. Lewis, published by Geoffrey Bles in 1950. It is the first published...</p>
              <div class="btn btn-sm btn-outline-secondary btn-white-style"><a href="blog_page_lion.aspx">Continue reading</a></div>
            </div>
           <img class="card-img-right flex-auto d-none d-md-block opacity-blog" src="imgs/blog2.jpg"/>
        </div>
                </div>
        <div class="col-md-12">
          <div class="card card-border flex-md-row mb-4 box-shadow h-md-250">
            <div class="card-body d-flex flex-column align-items-start">
              <strong class="d-inline-block mb-0 text-primary">12 February 2020</strong>
              <h3 class="mb-4">
                <a class="text-dark" href="#">The Witcher The Last Wish</a>
              </h3>
              <p class="card-text mb-auto"> is the first (in its fictional chronology; published second in original Polish) of the two collections of short stories (the other being Sword of Destiny)...</p>
              <div class="btn btn-sm btn-outline-secondary btn-white-style"><a href="blog_page_witcher.aspx">Continue reading</a></div>
            </div>
            <img class="card-img-right flex-auto d-none d-md-block opacity-blog" src="imgs/blog3.jpg"/>
          </div>
        </div>
        <div class="col-md-12">
          <div class="card card-border flex-md-row mb-4 box-shadow h-md-250">
            <div class="card-body d-flex flex-column align-items-start">
              <strong class="d-inline-block mb-0 text-primary">13 February 2020</strong>
              <h3 class="mb-4">
                <a class="text-dark" href="#">Yoga</a>
              </h3>
              <p class="card-text mb-auto">You should read this book if: You are new to the world of yoga or an experienced yogi. If you are looking for not only thorough descriptions...</p>
              <div class="btn btn-sm btn-outline-secondary btn-white-style"><a href="#">Continue reading</a></div>
            </div>
            <img class="card-img-right flex-auto d-none d-md-block opacity-blog" src="imgs/blog4.jpg"/>
        </div>
            </div>
            </div>
            </div>
    </section>
</main> 
</asp:Content>
