﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BoovieNested.Master" AutoEventWireup="true" CodeBehind="userlogin.aspx.cs" Inherits="BoovieStore.userlogin" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <main id="wrapper">
        <section class="has-margin-size-5">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 mx-auto">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <div class="element-center">
                                            <img width="150" src="imgs/nicolas.png"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="element-center">
                                            <h3>User login</h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <hr />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label>User ID</label>
                                        <div class="mb-3">
                                            <asp:TextBox class="form-control" ID="User_ID" runat="server" placeholder="Type in your ID"></asp:TextBox>
                                        </div>
                                        <label>Password</label>
                                        <div class="mb-3">
                                            <asp:TextBox class="form-control" ID="Password" runat="server" placeholder="Type in your password" TextMode="Password"></asp:TextBox>
                                        </div>
                                        <br />
                                        <div class="mb-3 d-grid">
                                            <asp:Button class="btn btn-success btn-lg w-100" ID="Login" runat="server" Text="Login" OnClick="Login_Click" />
                                        </div>
                                        <div class="mb-3 d-grid">
                                            <a href="signup.aspx"><input class="btn btn-info btn-lg w-100" id="Sign_Up" type="button" value="Sign Up" /></a>
                                        </div>
                                        <br />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="homepage.aspx" class="btn-back featurette-divider-bottom mt-4"><i class="fas fa-long-arrow-alt-left"></i> Back to Homepage</a>
                        <br />
                        <br />
                    </div>
                </div>
            </div>
        </section>
    </main>
</asp:Content>
